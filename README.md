# Python Code Generation

We try to generate Python Code using a description in natural language.
We're mainly focusing on two different methods using Transformers :
- Seq2seq generation
- AST (Abstract Syntax Tree) generation

## Installation

You first need to install the dependencies. We use Poetry to manage our package, if you don't have it, you need to run the following command.

```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
```

Then, you can install the venv with :

```bash
poetry install
```