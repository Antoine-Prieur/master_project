import ast
import io
import os
import pickle
import re
from tokenize import tokenize
from typing import Dict, List, Tuple

import astunparse
from asdl_pack.asdl import ASDLGrammar
from asdl_pack.hypothesis import Hypothesis
from asdl_pack.lang.py3.py3_asdl_helper import (
    asdl_ast_to_python_ast,
    python_ast_to_asdl_ast,
)
from asdl_pack.lang.py3.py3_transition_system import Python3TransitionSystem
from asdl_pack.transition_system import Action, ApplyRuleAction, GenTokenAction
from rich.progress import track
from typings import DataType
from utils.gft import add_root_index, add_sym_edges, edge_index_to_eigenvectors

from dataset.dataset import Vocabulary

ID_MODULE = 56
QUOTED_STRING_RE = re.compile(r"(?P<quote>['\"])(?P<string>.*?)(?<!\\)(?P=quote)")
VAR_RE = re.compile(r"(?:^|\W)a(?:$|\W)")


class DjangoDataset:
    def __init__(self, dataset_dir: str, vocab_file: str):
        assert os.path.isdir(dataset_dir)
        assert os.path.isfile(vocab_file)

        with open(os.path.join(dataset_dir, "train.bin"), "rb") as f:
            self.data_train = pickle.load(f)
        with open(os.path.join(dataset_dir, "valid.bin"), "rb") as f:
            self.data_valid = pickle.load(f)
        with open(os.path.join(dataset_dir, "test.bin"), "rb") as f:
            self.data_test = pickle.load(f)
        with open(vocab_file, "rb") as f:
            self.vocab = pickle.load(f)

    @classmethod
    def default_init(cls):
        assert os.path.isfile("./data/django1/train.bin")
        assert os.path.isfile("./data/django1/valid.bin")
        assert os.path.isfile("./data/django1/test.bin")
        assert os.path.isfile("./data/django1/vocab.bin")

        return cls(
            "./data/django1/",
            "./data/django1/vocab.bin",
        )

    @classmethod
    def default_init2(cls):
        assert os.path.isfile("./data/django2/train.bin")
        assert os.path.isfile("./data/django2/valid.bin")
        assert os.path.isfile("./data/django2/test.bin")
        assert os.path.isfile("./data/django2/vocab.bin")

        return cls(
            "./data/django2/",
            "./data/django2/vocab.bin",
        )

    @classmethod
    def default_init3(cls):
        assert os.path.isfile("./data/django3/train.bin")
        assert os.path.isfile("./data/django3/valid.bin")
        assert os.path.isfile("./data/django3/test.bin")
        assert os.path.isfile("./data/django3/vocab.bin")

        return cls(
            "./data/django3/",
            "./data/django3/vocab.bin",
        )

    def get_sets(self):
        return self.data_train, self.data_valid, self.data_test

    @staticmethod
    def replace_str(
        description: str, py_ast: ast.stmt
    ) -> Tuple[ast.stmt, str, Dict[str, str]]:
        str_map: Dict[str, str] = {}
        i = 0
        matches = QUOTED_STRING_RE.findall(description)

        for (quote, string) in matches:
            quoted_string = quote + string + quote
            if string not in str_map:
                str_map[string] = f"<STR:{i:02d}>"
                i += 1
            description = description.replace(quoted_string, str_map[string])

        for node in ast.walk(py_ast):
            if hasattr(node, "value"):
                value = node.value  # type: ignore
                if isinstance(value, str):
                    if value in str_map:
                        node.value = str_map[value]  # type: ignore
                    else:
                        idx = len(str_map)
                        str_map[value] = f"<STR:{idx:02d}>"
                        node.value = f"<STR:{idx:02d}>"  # type: ignore
        return py_ast, description, str_map

    @staticmethod
    def replace_var(
        description: str, actions: List[Action]
    ) -> Tuple[List[Action], str, Dict[str, str]]:
        count_id = 0
        var_map: Dict[str, str] = {}

        for action in actions:
            if isinstance(action, GenTokenAction):
                str_token = str(action.token)
                if (
                    str_token in description
                    and not re.fullmatch(r"<STR:\d{2}>", str_token)
                    and isinstance(action.token, str)
                ):
                    str_token = str(action.token)
                    if action.token in var_map:
                        action.token = var_map[str_token]
                    else:
                        var_map[str_token] = f"<VAR:{count_id:02d}>"
                        action.token = var_map[str_token]
                        count_id += 1

        for var, token in var_map.items():
            description = re.sub(
                rf"(^|\s|\.){var}(\.|\,|\s|$)",
                rf"\1{token}\2",
                description,
            )

        return actions, description, var_map

    @staticmethod
    def canonicalize_code(code: str):
        if re.match("elif(.*):$", code):
            code = "if True: pass\n" + code
        if re.match("else(.*):$", code):
            code = "if True: pass\n" + code
        if re.match("try(.*):$", code):
            code = code + "pass\nexcept: pass"
        elif re.match("except(.*):$", code):
            code = "try: pass\n" + code
        elif re.match("finally(.*):$", code):
            code = "try: pass\n" + code
        if re.match("^@(.*)", code):
            code = code + "\ndef dummy(): pass"
        if code[-1] == ":":
            code = code + " pass"
        return code

    @staticmethod
    def generate_dataset(
        anno_file: str,
        code_file: str,
        output_dir: str,
        grammar_file: str,
        vocab_file: str,
        enable_var: bool = False,
    ):
        """
        Set sanity_check to True the first time you execute the code
        """
        assert os.path.isdir(output_dir)
        assert os.path.isfile(anno_file)
        assert os.path.isfile(code_file)
        assert os.path.isfile(grammar_file)
        # assert os.path.isfile(vocab_file)

        grammar = ASDLGrammar.from_text(open(grammar_file).read())
        transition_system = Python3TransitionSystem(grammar)

        train = []
        valid = []
        test = []

        src_vocab = Vocabulary("Source natural language vocab")
        primitive_vocab = Vocabulary("Primitive vocab")
        source_code_vocab = Vocabulary("Source code vocabulary")

        for i, (description, code) in enumerate(
            track(list(zip(open(anno_file), open(code_file))))
        ):

            data: DataType = {}

            description = description.strip()

            code = DjangoDataset.canonicalize_code(code.strip())

            py_ast = ast.parse(code).body[0]

            py_ast, description, str_map = DjangoDataset.replace_str(
                description, py_ast
            )

            tgt_ast = python_ast_to_asdl_ast(py_ast, grammar)
            tgt_actions, edge_index = transition_system.get_actions(tgt_ast)
            transition_system.current_index = 0

            if enable_var:
                tgt_actions, description, var_map = DjangoDataset.replace_var(
                    description, tgt_actions
                )

                str_map = {v: k for k, v in dict(str_map, **var_map).items()}
            else:
                str_map = {v: k for k, v in dict(str_map).items()}
            edge_index = add_sym_edges(add_root_index(edge_index))
            eigen_vectors = edge_index_to_eigenvectors(edge_index, normalization="sym")

            # Check if the generated actions are correct
            hyp = Hypothesis()
            hyp.apply_action(ApplyRuleAction(grammar.id2prod[ID_MODULE]))

            for action in tgt_actions:
                assert (
                    action.__class__
                    in transition_system.get_valid_continuation_types(hyp)
                )
                if isinstance(action, ApplyRuleAction):
                    assert (
                        action.production
                        in transition_system.get_valid_continuating_productions(hyp)
                    )
                hyp.apply_action(action)

            tgt_code = astunparse.unparse(
                asdl_ast_to_python_ast(hyp.tree, grammar)
            ).strip()

            data["src_input"] = description
            data["tgt_code"] = tgt_code
            data["tgt_actions"] = tgt_actions
            data["str_map"] = str_map
            data["eigen_vectors"] = eigen_vectors

            if i < 16000:
                train.append(data)

                src_vocab.add_words(description)
                primitive_vocab.add_words(
                    [
                        action.token
                        for action in tgt_actions
                        if isinstance(action, GenTokenAction)
                    ]
                )

                source_code_vocab.add_words(
                    filter(
                        lambda x: x not in ["", "utf-8"],
                        map(
                            lambda x: x.string,
                            tokenize(io.BytesIO(tgt_code.encode("utf-8")).readline),
                        ),
                    )
                )

            elif i < 17000:
                valid.append(data)
            else:
                test.append(data)

        src_vocab.finalize(), primitive_vocab.finalize(), source_code_vocab.finalize()
        vocab = {
            "src_vocab": src_vocab,
            "primitive_vocab": primitive_vocab,
            "source_code_vocab": source_code_vocab,
        }

        with open(f"{os.path.join(output_dir, 'train.bin')}", "wb") as f:
            pickle.dump(train, f, pickle.HIGHEST_PROTOCOL)
        with open(f"{os.path.join(output_dir, 'valid.bin')}", "wb") as f:
            pickle.dump(valid, f, pickle.HIGHEST_PROTOCOL)
        with open(f"{os.path.join(output_dir, 'test.bin')}", "wb") as f:
            pickle.dump(test, f, pickle.HIGHEST_PROTOCOL)
        with open(f"{vocab_file}", "wb") as f:
            pickle.dump(vocab, f, pickle.HIGHEST_PROTOCOL)


if __name__ == "__main__":

    DjangoDataset.generate_dataset(
        "data/django/all.anno",
        "data/django/all.code",
        "data/django1",
        "asdl_pack/lang/py3/py385_asdl.txt",
        "data/django/vocab.bin",
        enable_var=False,
    )

    test = DjangoDataset.default_init()
    print(1)
