import inspect
import os
import sys
from collections import Counter
from types import ModuleType
from typing import List, Optional

import nltk
import torch
from asdl_pack.asdl import ASDLGrammar
from asdl_pack.transition_system import ApplyRuleAction, GenTokenAction, ReduceAction
from src.typings import DataType
from transformers import PreTrainedTokenizer, PreTrainedTokenizerFast
from utils.mask import (
    generate_padding_mask,
    generate_square_subsequent_mask,
    generate_subsequent_mask,
    hf_attention_mask,
)


class AnnotedCodeDataset(torch.utils.data.Dataset):
    def __init__(
        self,
        dataset_list: List[DataType],
        tokenizer: PreTrainedTokenizer,
        params: ModuleType,
        grammar: Optional[ASDLGrammar] = None,
    ):
        """[summary]

        Args:
            dataset_list ([type]): [description]
            tokenizer (PreTrainedTokenizer): tokenizer for the src and the primitive if AST
            params (Module): settings module
            grammar ([type], optional): [description]. Defaults to None.
            transform ([type], optional): [description]. Defaults to None.
        """
        self.data = dataset_list
        self.params = params
        if self.params.AST:  # type: ignore
            self.tokenizer = tokenizer
            self.grammar = grammar
        else:
            self.tokenizer = tokenizer

    def __len__(self):
        return len(self.data)

    def __getitem__(self, i: int):

        description = self.data[i]["src_input"]
        actions = self.data[i]["tgt_actions"]
        tgt_code = self.data[i]["tgt_code"]
        str_map = self.data[i]["str_map"]
        eigen_vectors = self.data[i]["eigen_vectors"]

        emb_description = []

        if (
            self.params.MODEL in ["bart", "marian", "bartLAP", "bartLAP2"]
            and self.params.PRETRAINED
        ):
            emb_description.extend(self.tokenizer.encode(description))
        else:
            emb_description.append(self.tokenizer.bos_token_id)
            emb_description.extend(self.tokenizer.encode(description))
            emb_description.append(self.tokenizer.eos_token_id)

        t_actions = []

        if self.params.AST:
            t_actions.append(self.tokenizer.bos_token_id)
            for action in actions:
                if isinstance(action, ApplyRuleAction):
                    t_actions.append(
                        self.tokenizer.convert_tokens_to_ids(
                            f"<NODE_{self.grammar.type2id[action.production.type]}_{self.grammar.prod2id[action.production]}>"
                        )
                    )
                elif isinstance(action, ReduceAction):
                    t_actions.append(self.tokenizer.convert_tokens_to_ids(f"<REDUCE>"))
                elif isinstance(action, GenTokenAction):
                    if action.token:
                        t_actions.append(
                            self.tokenizer.convert_tokens_to_ids(
                                self.tokenizer.tokenize(str(action.token))[0]
                            )
                        )
                    else:
                        t_actions.append(
                            self.tokenizer.convert_tokens_to_ids(
                                self.tokenizer.tokenize("<EMPTY>")[0]
                            )
                        )
                    # si le token est représenté avec plusieurs indices, on prend
                    # seulement le premier car un nom est censé
                    # représenter un indice dans la séquence

                else:
                    raise TypeError(f"{action} unknown")
            t_actions.append(self.tokenizer.eos_token_id)

        else:
            if (
                self.params.MODEL in ["bart", "marian", "bartLAP", "bartLAP2"]
                and self.params.PRETRAINED
            ):
                t_actions.extend(self.tokenizer.encode(tgt_code))
            else:
                t_actions.append(self.tokenizer.bos_token_id)
                t_actions.extend(self.tokenizer.encode(tgt_code))
                t_actions.append(self.tokenizer.eos_token_id)

        emb_description = torch.LongTensor(emb_description)
        t_actions = torch.LongTensor(t_actions)

        return emb_description, t_actions, eigen_vectors, str_map, tgt_code

    def tensor2description(self, tensor):
        """Return a description form a tensor

        Args:
            tensor (torch.tensor): SxN
        """
        res = []
        for i in range(tensor.size(1)):
            res.append(
                " ".join(
                    [
                        self.src_vocab.decode(tensor[j, i].item())
                        for j in range(tensor.size(0))
                    ]
                )
            )
        return res

    def tensor2rule_action(self, tensor, bs):
        """Return a sequence action from a tensor

        Args:
            tensor (torch.tensor): for bs = 2, tensor=[a1, b1, a2, b2, a3, b3,...]
            with ai the actions of the first AST, and bi the actions of the
            second AST
            bs (int): batch size

        Returns:
            [list]: list of bs lists representing the rule sequence
        """
        res = []
        for x in tensor:
            item = x.item()
            if item < len(self.grammar):
                res.append(self.grammar.id2prod[item])
            elif item == len(self.grammar):
                res.append(ReduceAction())
            else:
                res.append(self.primitive_vocab.decode(item - len(self.grammar) - 1))
        return [res[i::bs] for i in range(bs)]


class Vocabulary:
    def __init__(self, name):
        self.name = name
        self.words = []
        self.finalized = False

        self.word2id = {}
        self.word2id["<PAD>"] = 0
        self.word2id["<UNK>"] = 1
        self.word2id["<SOS>"] = 2
        self.word2id["<EOS>"] = 3

        self.id2word = {}

    # @classmethod
    # def from_list(cls, name, vocab_list):
    #    pass

    def __getitem__(self, word):
        return self.word2id.get(word, self.word2id["<UNK>"])

    def __len__(self):
        return len(self.word2id)

    def __contains__(self, word):
        return word in self.word2id

    def __str__(self):
        return f"Vocabulary (size = {len(self)}) {self.name}"

    def __add__(self, other):
        new_vocab = Vocabulary("new vocab")
        new_vocab.add_words(self.words)
        new_vocab.add_words(other.words)
        return new_vocab

    def decode(self, idx):
        return self.id2word[idx]

    def add_word(self, word):
        if word not in ["<PAD>", "<UNK>", "<SOS>", "<EOS>"]:
            self.words.append(word)
            self.finalized = False

    def add_words(self, words):
        self.finalized = False
        if isinstance(words, str):
            words = nltk.word_tokenize(words)
        for word in words:
            self.add_word(word)

    def tokenize(self, sentence):
        if isinstance(sentence, str):
            sentence = nltk.word_tokenize(sentence)
        tokenized = []
        for word in sentence:
            tokenized.append(self[word])
        return tokenized

    def finalize(self, max_size=None, freq_cut=10):
        top_k_words_cut = filter(
            lambda x: x[1] > freq_cut, Counter(self.words).most_common(max_size)
        )

        for word, _ in top_k_words_cut:
            self.word2id[word] = len(self.word2id)

        self.id2word = {word: idx for idx, word in self.word2id.items()}
        self.finalized = True


class AnnotedCodeCollator:
    def __init__(
        self,
        params,
        pad_id=None,
        de_pad_id=None,
        sep_id=None,
        valid=False,
        lap_pos_dim=0,
    ):
        """[summary]

        Args:
            params (Module): settings module
            pad_id ([type]): use only if you are not using an attention mask
            de_pad_id (int): use only if you are using a decoder and you are not using an attention mask
            sep_id ([type], optional): [description]. Defaults to None.
        """
        self.params = params
        self.pad_id = pad_id
        self.de_pad_id = de_pad_id
        self.sep_id = sep_id
        self.valid = valid
        self.lap_pos_dim = lap_pos_dim

    def __call__(self, batch):
        b_src_input, b_tgt_actions, b_eigen_vectors, str_map, tgt_code = zip(*batch)
        self.params.BATCH_SIZE = len(b_src_input)

        max_b_en_size = min(max(map(len, b_src_input)), self.params.MAX_INPUT_SIZE)
        max_b_de_size = min(max(map(len, b_tgt_actions)), self.params.MAX_INPUT_SIZE)

        if self.params.MODEL in [
            "default_transformer",
            "bart",
            "marian",
            "bartLAP",
            "bartLAP2",
        ]:

            en_in = torch.empty(self.params.BATCH_SIZE, max_b_en_size).fill_(
                self.pad_id
            )
            de_in = torch.empty(self.params.BATCH_SIZE, max_b_de_size - 1).fill_(
                self.de_pad_id
            )
            de_tgt = torch.empty(
                self.params.BATCH_SIZE, max_b_de_size - 1, dtype=torch.long
            ).fill_(self.de_pad_id)
            lap_pos_en = torch.zeros(
                (self.params.BATCH_SIZE, max_b_de_size - 1, self.lap_pos_dim)
            )

            for i, (src_input, tgt_actions, eigen_vectors) in enumerate(
                zip(b_src_input, b_tgt_actions, b_eigen_vectors)
            ):
                min_index_en = min(self.params.MAX_INPUT_SIZE, len(src_input))
                min_index_de = min(self.params.MAX_INPUT_SIZE, len(tgt_actions))

                en_in[i, :min_index_en] = src_input[:min_index_en]
                de_in[i, : min_index_de - 1] = tgt_actions[: min_index_de - 1]
                de_tgt[i, : min_index_de - 1] = tgt_actions[1:min_index_de]

                if self.params.MODEL in ["bartLAP", "bartLAP2"]:
                    lap_pos_en[
                        i,
                        : min_index_de - 1,
                        : min(eigen_vectors.shape[-1], self.lap_pos_dim),
                    ] = eigen_vectors[: min_index_de - 1, : self.lap_pos_dim]

        elif self.params.MODEL in ["gpt2"]:

            max_input_ids_size = min(
                max_b_en_size + max_b_de_size - 1, self.params.MAX_INPUT_SIZE
            )
            if self.valid:
                input_ids = torch.empty(self.params.BATCH_SIZE, max_b_en_size).fill_(
                    self.pad_id
                )
            else:
                input_ids = torch.empty(
                    self.params.BATCH_SIZE, max_input_ids_size
                ).fill_(self.pad_id)

            # token_ids_mask = torch.empty(
            #    self.params.BATCH_SIZE, max_input_ids_size, dtype=int
            # ).fill_(1)

            for i, (src_input, tgt_actions) in enumerate(
                zip(b_src_input, b_tgt_actions)
            ):
                min_index_src = min(self.params.MAX_INPUT_SIZE, len(src_input))
                min_index_tgt = min(self.params.MAX_INPUT_SIZE, len(tgt_actions))

                concat_tensor = torch.cat(
                    (
                        src_input[: min_index_src - 1],
                        torch.tensor([self.sep_id]),
                        tgt_actions[1:min_index_tgt],
                    )
                )
                len_concat = min(self.params.MAX_INPUT_SIZE, len(concat_tensor))

                if self.valid:
                    input_ids[i, :min_index_src] = concat_tensor[:min_index_src]
                else:
                    input_ids[i, :len_concat] = concat_tensor[:len_concat]
                # token_ids_mask[i, :min_index_src] = 0

        # On créé le dictionnaire qui sera donné en entrée du modèle

        data = {}
        if self.params.MODEL in ["default_transformer"]:
            en_in = torch.transpose(en_in.int(), 0, 1)
            de_in = torch.transpose(de_in.int(), 0, 1)
            data["en_in"] = en_in.to(self.params.DEVICE)
            data["de_in"] = de_in.to(self.params.DEVICE)

            en_pad_mask = generate_padding_mask(en_in, pad=self.pad_id)
            tgt_mask = (
                generate_square_subsequent_mask(de_in.size(0))
                if not self.params.CHEATER
                else None
            )
            padding_mask = generate_padding_mask(de_in, pad=self.de_pad_id)
            memory_mask = generate_subsequent_mask(de_in.size(0), en_in.size(0))

            data["en_pad_mask"] = en_pad_mask.to(self.params.DEVICE)
            data["tgt_mask"] = tgt_mask.to(self.params.DEVICE)
            data["attention_mask"] = padding_mask.to(self.params.DEVICE)
            data["memory_mask"] = memory_mask.to(self.params.DEVICE)
            if self.params.IGNORE_PAD_INDEX:
                data["labels"] = (
                    de_tgt.long()
                    .masked_fill(de_tgt == self.de_pad_id, -100)
                    .to(self.params.DEVICE)
                )
            else:
                data["labels"] = de_tgt.long().to(self.params.DEVICE)

        elif self.params.MODEL in ["bart", "marian", "bartLAP", "bartLAP2"]:
            data["input_ids"] = en_in.int().to(self.params.DEVICE)
            data["decoder_input_ids"] = de_in.int().to(self.params.DEVICE)
            data["attention_mask"] = hf_attention_mask(en_in, pad=self.pad_id).to(
                self.params.DEVICE
            )
            data["decoder_attention_mask"] = hf_attention_mask(
                de_in, pad=self.de_pad_id
            ).to(self.params.DEVICE)
            if self.params.IGNORE_PAD_INDEX:
                data["labels"] = (
                    de_tgt.long()
                    .masked_fill(de_tgt == self.de_pad_id, -100)
                    .to(self.params.DEVICE)
                )
            else:
                data["labels"] = de_tgt.long().to(self.params.DEVICE)

        elif self.params.MODEL in ["gpt2"]:
            data["input_ids"] = input_ids.int().to(self.params.DEVICE)
            data["attention_mask"] = hf_attention_mask(input_ids, pad=self.pad_id).to(
                self.params.DEVICE
            )
            # data["token_type_ids"] = token_ids_mask.to(self.params.DEVICE)
            if self.params.IGNORE_PAD_INDEX:
                data["labels"] = (
                    input_ids.long()
                    .masked_fill(input_ids == self.pad_id, -100)
                    .to(self.params.DEVICE)
                )
            else:
                data["labels"] = input_ids.long().to(self.params.DEVICE)

        # Si le modèle fait parti des modèles utilisant les matrices de Laplace
        # on ajoute les vecteurs propres de l'AST
        if self.params.MODEL in ["bartLAP", "bartLAP2"]:
            # On utilise le nom input_embeds pour utiliser les méthodes forward
            # déjà codées de HF sans avoir à rajouter un argument à chaque fois
            # La méthode forward de l'encodeur doit quant à elle être écrasée
            data["lap_pos_en"] = lap_pos_en.to(self.params.DEVICE)

        return data, str_map, tgt_code


if __name__ == "__main__":
    from settings import BATCH_SIZE, GRAMMAR_PATH
    from transformers import BartTokenizer

    from dataset.conala import ConalaSet
    from dataset.django import DjangoDataset

    conalaset = ConalaSet.default_init2()
    # djangoset = DjangoDataset.default_init()
    train_data, test_data = conalaset.get_sets()

    grammar = ASDLGrammar.from_text(open(GRAMMAR_PATH).read())

    """
    tokenizer = BartTokenizer.from_pretrained(
        "facebook/bart-base", local_files_only=True
    )
    """
    tokenizer = PreTrainedTokenizerFast(
        tokenizer_file=os.path.join(
            "weights",
            "tokenizer",
            "BPE_conala2_ASTTrue.json",
        ),
        bos_token="<SOS>",
        eos_token="<EOS>",
        pad_token="<PAD>",
    )

    # tokenizer.add_special_tokens({"pad_token": "<PAD>"})
    # tokenizer.add_special_tokens({"sep_token": "<SEP>"})

    class params:
        AST = True
        PRETRAINED = "facebook/bart-base"
        MODEL = "bart"
        BATCH_SIZE = 4
        MAX_INPUT_SIZE = 50
        CHEATER = False
        IGNORE_PAD_INDEX = True
        DEVICE = "cpu"

    train_set = AnnotedCodeDataset(test_data, tokenizer, params, grammar)

    collate_fn = AnnotedCodeCollator(
        params,
        pad_id=tokenizer.pad_token_id,
        valid=True,
        de_pad_id=tokenizer.pad_token_id,
        sep_id=tokenizer.sep_token_id,
        lap_pos_dim=8,
    )

    loader = torch.utils.data.DataLoader(
        dataset=train_set,
        batch_size=params.BATCH_SIZE,
        shuffle=False,
        collate_fn=collate_fn,
    )

    from tqdm import tqdm

    for x in tqdm(loader):
        pass

    print(1)
