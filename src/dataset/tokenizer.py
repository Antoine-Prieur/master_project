# ------------------------------------------------------------------------------
#
# Training of the tokenizer based on the settings file
#
# ------------------------------------------------------------------------------

import os
import re
from itertools import chain

from asdl_pack.asdl import ASDLGrammar
from asdl_pack.transition_system import GenTokenAction
from tokenizers import Tokenizer, models, pre_tokenizers

DATASET = "conala2"
AST = True
TOKENIZER = "BPE"
GRAMMAR_PATH = "asdl_pack/lang/py3/py385_asdl.txt"


if TOKENIZER == "BPE":
    tokenizer = Tokenizer(models.BPE())
else:
    raise ValueError(f"Tokenizer {TOKENIZER} not correct")


if DATASET == "django":
    from dataset.django import DjangoDataset

    dataset = DjangoDataset.default_init()
elif DATASET == "django2":
    from dataset.django import DjangoDataset

    dataset = DjangoDataset.default_init2()
elif DATASET == "django3":
    from dataset.django import DjangoDataset

    dataset = DjangoDataset.default_init3()
elif DATASET == "conala":
    from dataset.conala import ConalaSet

    dataset = ConalaSet.default_init()
    _, _, dataset_big = ConalaSet.default_init_mined().get_sets()
elif DATASET == "conala2":
    from dataset.conala import ConalaSet

    dataset = ConalaSet.default_init2()
    _, _, dataset_big = ConalaSet.default_init_mined().get_sets()
else:

    raise ValueError(f"Dataset {DATASET} not correct")

grammar = ASDLGrammar.from_text(open(GRAMMAR_PATH).read())

# Find the max number of strings into the set

x = list(
    map(
        lambda x: x["src_input"],
        chain(dataset.data_train, dataset.data_test),
    )
)

if DATASET in ["conala", "conala2"]:
    x += list(
        map(
            lambda x: x["src_input"],
            dataset_big,
        )
    )

if not AST:
    x += list(
        map(
            lambda x: x["tgt_code"],
            chain(dataset.data_train, dataset.data_test),
        )
    )

else:
    x += list(
        map(
            lambda action: str(action.token),
            filter(
                lambda action: isinstance(action, GenTokenAction),
                chain.from_iterable(
                    map(
                        lambda x: x["tgt_actions"],
                        chain(dataset.data_train, dataset.data_test, dataset_big),
                    )
                ),
            ),
        )
    )


max_str = max(map(lambda x: int(x), re.findall(r"<STR:([0-9][0-9])>", "".join(x))))
if DATASET in ["django2", "django3", "conala2"]:
    max_var = max(map(lambda x: int(x), re.findall(r"<VAR:([0-9][0-9])>", "".join(x))))


# Training
tokenizer.pre_tokenizer = pre_tokenizers.Whitespace()
tokenizer.add_tokens(["<PAD>", "<SOS>", "<EOS>"])

if AST:
    tokenizer.add_tokens(
        [
            f"<NODE_{grammar.type2id[value.type]}_{key}>"
            for key, value in grammar.id2prod.items()
        ]
    )
    tokenizer.add_tokens(["<REDUCE>"])
else:
    tokenizer.add_tokens(["<SEP>"])
tokenizer.add_tokens([f"<STR:{i:02}>" for i in range(max_str + 1)])

if DATASET in ["django2", "django3", "conala2"]:
    tokenizer.add_tokens([f"<VAR:{i:02}>" for i in range(max_var + 1)])

if DATASET in ["conala", "conala2"]:
    tokenizer.add_tokens(
        ["<EMPTY>", " ", "\n", "\r\n", "\n\t", "\n\r", "\t\n", "\t", "\r", " \t\n\r"]
    )

tokenizer.train_from_iterator(x, length=len(x))
tokenizer.save(
    os.path.join("weights", "tokenizer", f"{TOKENIZER}_{DATASET}_AST{AST}.json")
)
print(
    f"Les poids ont bien été sauvegardé dans weights/tokenizer/{TOKENIZER}_{DATASET}_AST{AST}.json"
)
