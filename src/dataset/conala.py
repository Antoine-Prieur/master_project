import ast
import json
import os
import pickle
import re
from typing import Dict, List, Tuple

import astunparse
from asdl_pack.asdl import ASDLGrammar
from asdl_pack.hypothesis import Hypothesis
from asdl_pack.lang.py3.py3_asdl_helper import (
    asdl_ast_to_python_ast,
    python_ast_to_asdl_ast,
)
from asdl_pack.lang.py3.py3_transition_system import Python3TransitionSystem
from asdl_pack.transition_system import Action, ApplyRuleAction, GenTokenAction
from rich.progress import track
from typings import ConalaType, DataType
from utils.gft import add_root_index, add_sym_edges, edge_index_to_eigenvectors

ID_MODULE = 56
QUOTED_STRING_RE = re.compile(r"(?P<quote>['])(?P<string>.*?)(?<!\\)(?P=quote)")
VAR_RE = re.compile(r"(?:^|\W)a(?:$|\W)")


class ConalaSet:
    def __init__(self, dataset_dir: str, mined: bool = False):
        assert os.path.isdir(dataset_dir)

        self.mined = mined

        with open(os.path.join(dataset_dir, "train.bin"), "rb") as f:
            self.data_train = pickle.load(f)
        with open(os.path.join(dataset_dir, "test.bin"), "rb") as f:
            self.data_test = pickle.load(f)
        if mined:
            with open(os.path.join(dataset_dir, "conala_mined.bin"), "rb") as f:
                self.data_mined = pickle.load(f)

    @classmethod
    def default_init(cls):
        assert os.path.isfile("./data/conala/train.bin")
        assert os.path.isfile("./data/conala/test.bin")

        return cls(
            "./data/conala/",
        )

    @classmethod
    def default_init2(cls):
        assert os.path.isfile("./data/conala2/train.bin")
        assert os.path.isfile("./data/conala2/test.bin")

        return cls(
            "./data/conala2/",
        )

    @classmethod
    def default_init_mined(cls):
        assert os.path.isfile("./data/conala/train.bin")
        assert os.path.isfile("./data/conala/test.bin")
        assert os.path.isfile("./data/conala/conala_mined.bin")

        return cls("./data/conala/", mined=True)

    def get_sets(self):
        if self.mined:
            return self.data_train, self.data_test, self.data_mined
        return self.data_train, self.data_test

    @staticmethod
    def replace_str(
        description: str, py_ast: ast.stmt
    ) -> Tuple[ast.stmt, str, Dict[str, str]]:
        str_map: Dict[str, str] = {}
        i = 0
        matches = QUOTED_STRING_RE.findall(description)

        for (_, string) in matches:
            if string not in str_map:
                escaped_string = re.sub(r"\\\\", r"\\", string)
                str_map[escaped_string] = f"<STR:{i:02d}>"
            description = re.sub(
                rf"""('|"){re.escape(string)}('|")""", f"<STR:{i:02d}>", description
            )
            i += 1

        for node in ast.walk(py_ast):
            if hasattr(node, "value"):
                value: ast.expr = node.value  # type: ignore
                if isinstance(value, str):
                    if value in str_map:
                        node.value = str_map[value]  # type: ignore

        return py_ast, description, str_map

    @staticmethod
    def replace_var(
        description: str, actions: List[Action]
    ) -> Tuple[List[Action], str, Dict[str, str]]:
        count_id = 0
        var_map: Dict[str, str] = {}

        for action in actions:
            if isinstance(action, GenTokenAction):
                str_token = str(action.token)
                if (
                    re.findall(rf"('|`){re.escape(str_token)}('|`)", description)
                    and not re.fullmatch(r"<STR:\d{2}>", str_token)
                    and isinstance(action.token, str)
                ):
                    if action.token in var_map:
                        action.token = var_map[str_token]
                    else:
                        var_map[str_token] = f"<VAR:{count_id:02d}>"
                        action.token = var_map[str_token]
                        count_id += 1

        for var, token in var_map.items():
            description = re.sub(
                rf"(^|\s|\.|`){var}(\.|\,|`|\s|$)",
                rf"\1{token}\2",
                description,
            )

        return actions, description, var_map

    @staticmethod
    def canonicalize_code(code: str):
        if re.match("elif(.*):$", code):
            code = "if True: pass\n" + code
        if re.match("else(.*):$", code):
            code = "if True: pass\n" + code
        if re.match("try(.*):$", code):
            code = code + "pass\nexcept: pass"
        elif re.match("except(.*):$", code):
            code = "try: pass\n" + code
        elif re.match("finally(.*):$", code):
            code = "try: pass\n" + code
        if re.match("^@(.*)", code):
            code = code + "\ndef dummy(): pass"
        if code[-1] == ":":
            code = code + " pass"
        return code

    @staticmethod
    def generate_dataset(
        train_file: str,
        test_file: str,
        output_dir: str,
        grammar_file: str,
        enable_var: bool = False,
    ) -> None:

        assert os.path.isdir(output_dir)
        assert os.path.isfile(grammar_file)

        grammar = ASDLGrammar.from_text(open(grammar_file).read())
        transition_system = Python3TransitionSystem(grammar)

        with open(train_file, "r") as f:
            train_content: List[ConalaType] = json.loads(f.read())

        with open(test_file, "r") as f:
            test_content: List[ConalaType] = json.loads(f.read())

        train: List[DataType] = []
        test: List[DataType] = []

        # Création du trainset
        for example in track(train_content, description="Processing..."):

            if example["rewritten_intent"] is not None:
                description: str = example["rewritten_intent"].strip()
            else:
                description = example["intent"].strip()
            code = ConalaSet.canonicalize_code(example["snippet"].strip())

            py_ast = ast.parse(code).body[0]

            py_ast, description, str_map = ConalaSet.replace_str(description, py_ast)

            tgt_ast = python_ast_to_asdl_ast(py_ast, grammar)
            tgt_actions, edge_index = transition_system.get_actions(tgt_ast)
            transition_system.current_index = 0

            if enable_var:
                tgt_actions, description, var_map = ConalaSet.replace_var(
                    description, tgt_actions
                )

                str_map = {v: k for k, v in dict(str_map, **var_map).items()}
            else:
                str_map = {v: k for k, v in dict(str_map).items()}

            edge_index = add_sym_edges(add_root_index(edge_index))
            eigen_vectors = edge_index_to_eigenvectors(edge_index, normalization="sym")

            # Check if the generated actions are correct
            hyp = Hypothesis()
            hyp.apply_action(ApplyRuleAction(grammar.id2prod[ID_MODULE]))

            for action in tgt_actions:
                assert (
                    action.__class__
                    in transition_system.get_valid_continuation_types(hyp)
                )
                if isinstance(action, ApplyRuleAction):
                    assert (
                        action.production
                        in transition_system.get_valid_continuating_productions(hyp)
                    )
                hyp.apply_action(action)

            tgt_code = astunparse.unparse(
                asdl_ast_to_python_ast(hyp.tree, grammar)
            ).strip()

            data: DataType = {
                "src_input": description,
                "tgt_code": tgt_code,
                "tgt_actions": tgt_actions,
                "str_map": str_map,
                "eigen_vectors": eigen_vectors,
            }

            train.append(data)

        # Création du testset (exactement pareil qu'au dessus)
        for example in track(test_content, description="Processing..."):

            if example["rewritten_intent"] is not None:
                description = example["rewritten_intent"].strip()
            else:
                description = example["intent"].strip()
            code = ConalaSet.canonicalize_code(example["snippet"].strip())

            py_ast = ast.parse(code).body[0]

            py_ast, description, str_map = ConalaSet.replace_str(description, py_ast)

            tgt_ast = python_ast_to_asdl_ast(py_ast, grammar)
            tgt_actions, edge_index = transition_system.get_actions(tgt_ast)
            transition_system.current_index = 0

            if enable_var:
                tgt_actions, description, var_map = ConalaSet.replace_var(
                    description, tgt_actions
                )

                str_map = {v: k for k, v in dict(str_map, **var_map).items()}
            else:
                str_map = {v: k for k, v in dict(str_map).items()}

            edge_index = add_sym_edges(add_root_index(edge_index))
            eigen_vectors = edge_index_to_eigenvectors(edge_index, normalization="sym")

            # Check if the generated actions are correct
            hyp = Hypothesis()
            hyp.apply_action(ApplyRuleAction(grammar.id2prod[ID_MODULE]))

            for action in tgt_actions:
                assert (
                    action.__class__
                    in transition_system.get_valid_continuation_types(hyp)
                )
                if isinstance(action, ApplyRuleAction):
                    assert (
                        action.production
                        in transition_system.get_valid_continuating_productions(hyp)
                    )
                hyp.apply_action(action)

            tgt_code = astunparse.unparse(
                asdl_ast_to_python_ast(hyp.tree, grammar)
            ).strip()

            data = {
                "src_input": description,
                "tgt_code": tgt_code,
                "tgt_actions": tgt_actions,
                "str_map": str_map,
                "eigen_vectors": eigen_vectors,
            }

            test.append(data)

        with open(f"{os.path.join(output_dir, 'train.bin')}", "wb") as file1:
            pickle.dump(train, file1, pickle.HIGHEST_PROTOCOL)
        with open(f"{os.path.join(output_dir, 'test.bin')}", "wb") as file1:
            pickle.dump(test, file1, pickle.HIGHEST_PROTOCOL)

    @staticmethod
    def generate_mined_dataset(
        content_file: str,
        output_dir: str,
        grammar_file: str,
    ) -> None:

        assert os.path.isdir(output_dir)
        assert os.path.isfile(grammar_file)

        grammar = ASDLGrammar.from_text(open(grammar_file).read())
        transition_system = Python3TransitionSystem(grammar)

        content: List[ConalaType] = []

        for line in open(content_file, "r"):
            content.append(json.loads(line))

        dataset: List[DataType] = []

        # Création du trainset
        for example in track(content, description="Processing..."):

            if example["snippet"] and example["intent"]:

                description = example["intent"].strip()
                code = ConalaSet.canonicalize_code(example["snippet"].strip())

                py_ast = ast.parse(code).body[0]

                py_ast, description, str_map = ConalaSet.replace_str(
                    description, py_ast
                )
                str_map = {v: k for k, v in dict(str_map).items()}

                tgt_ast = python_ast_to_asdl_ast(py_ast, grammar)
                tgt_actions, edge_index = transition_system.get_actions(tgt_ast)
                transition_system.current_index = 0

                edge_index = add_sym_edges(add_root_index(edge_index))
                eigen_vectors = edge_index_to_eigenvectors(
                    edge_index, normalization="sym"
                )

                # Check if the generated actions are correct
                hyp = Hypothesis()
                hyp.apply_action(ApplyRuleAction(grammar.id2prod[ID_MODULE]))

                for action in tgt_actions:
                    assert (
                        action.__class__
                        in transition_system.get_valid_continuation_types(hyp)
                    )
                    if isinstance(action, ApplyRuleAction):
                        assert (
                            action.production
                            in transition_system.get_valid_continuating_productions(hyp)
                        )
                    hyp.apply_action(action)

                tgt_code = astunparse.unparse(
                    asdl_ast_to_python_ast(hyp.tree, grammar)
                ).strip()

                data: DataType = {
                    "src_input": description,
                    "tgt_code": tgt_code,
                    "tgt_actions": tgt_actions,
                    "str_map": str_map,
                    "eigen_vectors": eigen_vectors,
                }

                dataset.append(data)

        with open(f"{os.path.join(output_dir, 'conala_mined.bin')}", "wb") as f:
            pickle.dump(dataset, f, pickle.HIGHEST_PROTOCOL)


if __name__ == "__main__":

    ConalaSet.generate_dataset(
        "data/conala/conala-train.json",
        "data/conala/conala-test.json",
        "data/conala2",
        "asdl_pack/lang/py3/py385_asdl.txt",
        enable_var=True,
    )

    test = ConalaSet.default_init2()
    print(1)
