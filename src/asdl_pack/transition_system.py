# coding=utf-8


from typing import List, Tuple, Union


class Action(object):
    pass


class ApplyRuleAction(Action):
    def __init__(self, production):
        self.production = production

    def __hash__(self):
        return hash(self.production)

    def __eq__(self, other):
        return (
            isinstance(other, ApplyRuleAction) and self.production == other.production
        )

    def __ne__(self, other):
        return not self.__eq__(other)

    def __repr__(self):
        return "ApplyRule[%s]" % self.production.__repr__()


class GenTokenAction(Action):
    def __init__(self, token: Union[int, str]):
        self.token = token

    def is_stop_signal(self):
        return self.token == "</primitive>"

    def __repr__(self):
        return "GenToken[%s]" % self.token


class ReduceAction(Action):
    def __repr__(self):
        return "Reduce"


class PadAction(Action):
    def __repr__(self):
        return "Padding"


class TransitionSystem(object):
    def __init__(self, grammar):
        self.grammar = grammar
        self.current_index = 0

    def get_actions(self, asdl_ast, index=0) -> Tuple[List[Action], List[List[int]]]:
        """
        generate action sequence given the ASDL Syntax Tree
        """

        actions = []
        edge_index = []

        parent_action = ApplyRuleAction(asdl_ast.production)
        actions.append(parent_action)

        for field in asdl_ast.fields:
            # is a composite field
            if self.grammar.is_composite_type(field.type):
                if field.cardinality == "single":
                    self.current_index += 1
                    edge_index.append([index, self.current_index])
                    field_actions, edge = self.get_actions(
                        field.value, self.current_index
                    )
                    edge_index = edge_index + edge
                else:
                    field_actions = []

                    if field.value is not None:
                        if field.cardinality == "multiple":
                            for val in field.value:
                                self.current_index += 1
                                edge_index.append([index, self.current_index])

                                cur_child_actions, edge = self.get_actions(
                                    val, self.current_index
                                )
                                field_actions.extend(cur_child_actions)

                                edge_index = edge + edge_index
                        elif field.cardinality == "optional":
                            self.current_index += 1
                            edge_index.append([index, self.current_index])

                            field_actions, edge = self.get_actions(
                                field.value, self.current_index
                            )

                            edge_index = edge_index + edge

                    # if an optional field is filled, then do not need Reduce action
                    if (
                        field.cardinality == "multiple"
                        or field.cardinality == "optional"
                        and not field_actions
                    ):
                        self.current_index += 1
                        edge_index.append([index, self.current_index])
                        field_actions.append(ReduceAction())

            else:  # is a primitive field

                field_actions = self.get_primitive_field_actions(field)

                for _ in field_actions:
                    self.current_index += 1
                    edge_index.append([index, self.current_index])

                # if an optional field is filled, then do not need Reduce action
                if (
                    field.cardinality == "multiple"
                    or field.cardinality == "optional"
                    and not field_actions
                ):
                    self.current_index += 1
                    edge_index.append([index, self.current_index])
                    # reduce action
                    field_actions.append(ReduceAction())

            actions.extend(field_actions)

        return actions, edge_index

    def get_edge_index(self, asdl_ast, index=0):
        """
        generate edge index : word when the tree is not completed
        """

        actions = []
        edge_index = []

        parent_action = ApplyRuleAction(asdl_ast.production)
        actions.append(parent_action)

        for field in asdl_ast.fields:
            # is a composite field
            if self.grammar.is_composite_type(field.type):
                if field.cardinality == "single":
                    try:
                        self.current_index += 1
                        edge_index.append([index, self.current_index])
                        field_actions, edge = self.get_edge_index(
                            field.value, self.current_index
                        )
                        edge_index = edge_index + edge
                    except:
                        pass
                else:
                    field_actions = []

                    if field.value is not None:
                        if field.cardinality == "multiple":
                            for val in field.value:

                                try:
                                    self.current_index += 1
                                    edge_index.append([index, self.current_index])
                                    cur_child_actions, edge = self.get_edge_index(
                                        val, self.current_index
                                    )
                                    field_actions.extend(cur_child_actions)
                                    edge_index = edge + edge_index
                                except:
                                    pass

                        elif field.cardinality == "optional":
                            try:
                                self.current_index += 1
                                edge_index.append([index, self.current_index])

                                field_actions, edge = self.get_edge_index(
                                    field.value, self.current_index
                                )

                                edge_index = edge_index + edge
                            except:
                                pass

                    # if an optional field is filled, then do not need Reduce action
                    if (
                        field.cardinality == "multiple"
                        or field.cardinality == "optional"
                        and not field_actions
                    ):
                        self.current_index += 1
                        edge_index.append([index, self.current_index])
                        field_actions.append(ReduceAction())

            else:  # is a primitive field
                self.current_index += 1
                edge_index.append([index, self.current_index])

                field_actions = self.get_primitive_field_actions(field)

                # if an optional field is filled, then do not need Reduce action
                if (
                    field.cardinality == "multiple"
                    or field.cardinality == "optional"
                    and not field_actions
                ):
                    # reduce action
                    field_actions.append(ReduceAction())

            actions.extend(field_actions)

        return actions, edge_index

    def tokenize_code(self, code, mode):
        raise NotImplementedError

    def compare_ast(self, hyp_ast, ref_ast):
        raise NotImplementedError

    def ast_to_surface_code(self, asdl_ast):
        raise NotImplementedError

    def surface_code_to_ast(self, code):
        raise NotImplementedError

    def get_primitive_field_actions(self, realized_field):
        raise NotImplementedError

    def get_valid_continuation_types(self, hyp):
        if hyp.tree:
            if self.grammar.is_composite_type(hyp.frontier_field.type):
                if hyp.frontier_field.cardinality == "single":
                    return (ApplyRuleAction,)
                else:  # optional, multiple
                    return ApplyRuleAction, ReduceAction
            else:
                if hyp.frontier_field.cardinality == "single":
                    return (GenTokenAction,)
                elif hyp.frontier_field.cardinality == "optional":
                    if hyp._value_buffer:
                        return (GenTokenAction,)
                    else:
                        return GenTokenAction, ReduceAction
                else:
                    return GenTokenAction, ReduceAction
        else:
            return (ApplyRuleAction,)

    def get_valid_continuating_productions(self, hyp):
        if hyp.tree:
            if self.grammar.is_composite_type(hyp.frontier_field.type):
                return self.grammar[hyp.frontier_field.type]
            else:
                raise ValueError
        else:
            return self.grammar[self.grammar.root_type]

    @staticmethod
    def get_class_by_lang(lang):
        if lang == "python":
            from .lang.py.py_transition_system import PythonTransitionSystem

            return PythonTransitionSystem
        elif lang == "python3":
            from .lang.py3.py3_transition_system import Python3TransitionSystem

            return Python3TransitionSystem

        raise ValueError("unknown language %s" % lang)
