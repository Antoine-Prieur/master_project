from typing import Dict, List, Literal, TypedDict, Union

from torch.functional import Tensor
from transformers.models.bart.tokenization_bart import BartTokenizer
from transformers.models.gpt2.tokenization_gpt2 import GPT2Tokenizer
from transformers.tokenization_utils import PreTrainedTokenizer

from asdl_pack.transition_system import Action

# SETTINGS TYPINGS

ModelType = Literal["gpt2", "bart", "bartLAP", "bartLAP2", "bartLAP3"]
DatasetType = Literal["django", "django2", "django3", "conala", "conala2"]
PretrainedType = Literal["gpt2", "facebook/bart-base", "facebook/bart-large"]
SchedulerType = Literal["linear", "constant", "cosine", "hardcosine"]
TokenizerType = Literal["BPE"]


# TORCH and HF TYPINGS

HFTokenizerType = Union[GPT2Tokenizer, BartTokenizer, PreTrainedTokenizer]

# DATASET TYPINGS


class DataType(TypedDict):
    src_input: str
    tgt_code: str
    tgt_actions: List[Action]
    str_map: Dict[str, str]
    eigen_vectors: Tensor


class ConalaType(TypedDict):
    intent: str
    question_id: int
    rewritten_intent: str
    snippet: str


class ConalaMinedType(TypedDict):
    intent: str
    parent_answer_post_id: int
    question_id: int
    prob: float
    snippet: str
