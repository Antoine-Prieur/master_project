from typing import List, Literal

import numpy as np
import torch
from torch.functional import Tensor
from torch_geometric.utils import get_laplacian, to_scipy_sparse_matrix

NormalizationType = Literal["sym", "rw", None]


def edge_index_to_eigenvectors(
    edge_index: List[List[int]], normalization: NormalizationType = None
) -> Tensor:

    if not edge_index:
        return torch.zeros((0, 0)).float()

    edge_index = torch.tensor(edge_index, dtype=torch.long)
    edge_index, edge_attr = get_laplacian(
        edge_index.t().contiguous(), None, normalization=normalization
    )
    L = to_scipy_sparse_matrix(edge_index, edge_attr).tocsc()
    EigVal, EigVec = np.linalg.eig(L.toarray())

    # On trie les vecteurs propres selon les valeurs propres
    idx = EigVal.argsort()
    EigVal, EigVec = EigVal[idx], np.real(EigVec[:, idx])

    return torch.from_numpy(
        EigVec[:, 1:]
    ).float()  # on ne prend pas la première valeur propre car toujours 0


def remove_root_index(edge_index: List[List[int]]) -> List[List[int]]:
    """Prend la liste edge_index calculée dans hypothesis, et enlève les
    arrêtes du noeud initial, en réindexant les autres

    Args:
        edge_index (list): edge index
    """
    res: List[List[int]] = []
    for i, j in edge_index:
        if i != 0:
            res.append([i - 1, j - 1])
    return res


def add_root_index(edge_index: List[List[int]]) -> List[List[int]]:
    res: List[List[int]] = []
    max_node = 0
    for i, j in edge_index:
        if i + 1 > max_node:
            max_node = i + 1
        if j + 1 > max_node:
            max_node = j + 1
        res.append([i + 1, j + 1])
    res.append([0, 1])
    res.append([0, max_node + 1])
    return res


def add_sym_edges(edge_index: List[List[int]]) -> List[List[int]]:
    res: List[List[int]] = []
    for i, j in edge_index:
        res.append([i, j])
        res.append([j, i])
    return res
