import matplotlib.pyplot as plt
from dataset.conala import ConalaSet
from dataset.django import DjangoDataset

django = DjangoDataset.default_init()
django_train = django.data_train

conala = ConalaSet.default_init()
conala_train = conala.data_train

django_list = list(map(lambda x: len(x["tgt_actions"]), django_train))
conala_list = list(map(lambda x: len(x["tgt_actions"]), conala_train))

django_list = [x for x in django_list if x < 300]
conala_list = [x for x in conala_list if x < 300]


fig, ax = plt.subplots()

ax.hist(django_list, bins=30, alpha=0.5, label="django", color="orange")
ax.set_ylabel("django", color="orange")

ax2 = ax.twinx()

ax2.hist(conala_list, bins=30, alpha=0.5, label="conala", color="blue")
ax2.set_ylabel("conala", color="blue")

plt.show()
