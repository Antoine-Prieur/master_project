# ============================================================================ #
#
#                 BEAM SEARCH FOR ASTs
#
# ============================================================================ #

import inspect
import os
import re
import sys

import numpy as np
import torch
import torch.nn.functional as F
from asdl_pack.hypothesis import *
from asdl_pack.lang.py3.py3_transition_system import Python3TransitionSystem
from asdl_pack.transition_system import ApplyRuleAction, GenTokenAction, ReduceAction
from models.bartLAP2 import get_bartLAP2
from transformers import PreTrainedTokenizer

from utils.gft import edge_index_to_eigenvectors


class BeamSearch:
    def __init__(
        self, tokenizer: PreTrainedTokenizer, grammar, lap_pos_dim=None, device="cpu"
    ):

        self.tokenizer = tokenizer
        self.grammar = grammar
        self.parser = Python3TransitionSystem(grammar)
        self.bos = tokenizer.bos_token_id
        self.eos = tokenizer.eos_token_id
        self.pad = tokenizer.pad_token_id
        self.device = device
        self.lap_pos_dim = lap_pos_dim

        self.vocab_length = len(tokenizer)

        self.masks = {
            key: torch.zeros(self.vocab_length).to(device)
            for key in grammar.id2type.keys()
        }
        self.masks["Leaves"] = torch.ones(self.vocab_length).to(device)
        self.masks["Reduce"] = torch.zeros(self.vocab_length).to(device)

        for key, value in grammar.id2prod.items():
            node_id = self.tokenizer.convert_tokens_to_ids(
                f"<NODE_{grammar.type2id[value.type]}_{key}>"
            )
            self.masks[grammar.type2id[value.type]][node_id] = 1
            self.masks["Leaves"][node_id] = 0

        reduce_id = self.tokenizer.convert_tokens_to_ids("<REDUCE>")
        self.masks["Reduce"][reduce_id] = 1
        self.masks["Leaves"][reduce_id] = 0
        self.masks["Leaves"][self.bos] = 0
        self.masks["Leaves"][self.eos] = 0
        self.masks["Leaves"][self.pad] = 0

    def generate(
        self,
        model,
        encoder_inputs,
        max_length=100,
        num_beams=4,
        num_returned_sequence=1,
    ):
        """Fonction à utiliser pour générer des AST, rajoute un masque à chaque étape
            afin de chercher le noeud parmi le bon type

        Args:
            model
            encoder_inputs (dict): input_ids, attention_mask ...
            max_length (int, optional): max length of the generated ouput. Defaults to 100.
            num_beams (int, optional): number of beams. Defaults to 4.
            num_returned_sequence (int, optional): number of returned sequence. Defaults to 1.
        """

        if num_beams < num_returned_sequence:
            raise ValueError(
                "Le nombre de séquences générées doit être inférieure ou égale au nombre de beams"
            )

        self.model = model
        self.encoder_inputs = encoder_inputs
        self.num_beams = num_beams

        batch_size = encoder_inputs["input_ids"].size(0)

        res = []

        for i in range(batch_size):
            beam_batch = (torch.ones((1, 1), dtype=int) * self.bos).to(self.device)
            beam_score = torch.zeros((1, 1)).to(self.device)
            hypothesis = Hypothesis()
            hypothesis.apply_action(ApplyRuleAction(self.grammar.id2prod[56]))
            beam_hyp = [hypothesis.copy() for _ in range(num_beams)]

            for j in range(max_length):
                beam_batch, beam_score, beam_hyp = self.beam_step(
                    beam_batch, beam_score, beam_hyp, i
                )
                if all(
                    torch.eq(beam_batch[:num_returned_sequence, -1], self.eos)
                    + torch.eq(beam_batch[:num_returned_sequence, -1], self.pad)
                ):
                    break
            res.append(
                (
                    beam_batch[:num_returned_sequence],
                    beam_score[:num_returned_sequence],
                    beam_hyp[:num_returned_sequence],
                )
            )

        return res

    def beam_step(self, decoder_inputs, beam_score, beam_hyp, batch_index):

        encoder_inputs = self.encoder_inputs["input_ids"][batch_index].repeat(
            decoder_inputs.size(0), 1
        )
        attention_mask = self.encoder_inputs["attention_mask"][batch_index].repeat(
            decoder_inputs.size(0), 1
        )
        if self.lap_pos_dim is not None:
            lap_pos_en = torch.zeros(
                (decoder_inputs.size(0), decoder_inputs.size(1), self.lap_pos_dim)
            ).to(self.device)

            eigen_vectors = []
            for hyp in beam_hyp:
                eigen_vectors.append(edge_index_to_eigenvectors(hyp.edge_index))

            for i in range(decoder_inputs.size(0)):
                lap_pos_en[
                    i,
                    : eigen_vectors[i].size(0),
                    : min(eigen_vectors[i].size(-1), self.lap_pos_dim),
                ] = eigen_vectors[i][:, : self.lap_pos_dim]

            logits = F.softmax(
                self.model(
                    input_ids=encoder_inputs,
                    lap_pos_en=lap_pos_en,
                    attention_mask=attention_mask,
                    decoder_input_ids=decoder_inputs,
                ).logits,
                dim=-1,
            )[:, -1]
        else:
            logits = F.softmax(
                self.model(
                    input_ids=encoder_inputs,
                    attention_mask=attention_mask,
                    decoder_input_ids=decoder_inputs,
                ).logits,
                dim=-1,
            )[:, -1]

        # Si le dernier token était EOS, on met toutes les probabilités à 0
        # ssi on n'est pas à la première itération avec seulement un BOS
        # (permet d'éviter des problèmes si EOS = BOS)

        if decoder_inputs.size(1) != 1:
            mask = torch.zeros((self.num_beams, self.vocab_length)).to(self.device)

            for i in range(self.num_beams):
                # Si l'AST est totalement fini (AST + EOS), on veut rajouter des
                # pad tant que les autres séquences ne sont pas générées
                # On met la proba à 1 pour garder la proba de la séquence
                if (
                    decoder_inputs[i, -1] == self.eos
                    or decoder_inputs[i, -1] == self.pad
                ):

                    logits[i][self.pad] = 1
                    mask[i][self.pad] = 1

                # Si l'AST n'est pas fini, on créé les masques de type
                elif not beam_hyp[i].completed:
                    valid_types = self.parser.get_valid_continuation_types(beam_hyp[i])

                    if ApplyRuleAction in valid_types:
                        mask[i] = (
                            mask[i]
                            + self.masks[
                                self.grammar.type2id[
                                    self.parser.get_valid_continuating_productions(
                                        beam_hyp[i]
                                    )[0].type
                                ]
                            ]
                        )
                    if GenTokenAction in valid_types:
                        mask[i] = mask[i] + self.masks["Leaves"]
                    if ReduceAction in valid_types:
                        if beam_hyp[i].clone_and_apply_action(ReduceAction()).completed:
                            mask[i] = self.masks["Reduce"]
                        else:
                            mask[i] = mask[i] + self.masks["Reduce"]

                # Si l'AST est fini, on veut rajouter un EOS
                # On met la proba à 1 pour garder la proba de l'AST
                else:
                    logits[i][self.eos] = 1
                    mask[i][self.eos] = 1
        else:
            mask = torch.zeros((1, self.vocab_length)).to(self.device)
            valid_types = self.parser.get_valid_continuation_types(beam_hyp[0])

            if ApplyRuleAction in valid_types:
                mask = (
                    mask
                    + self.masks[
                        self.grammar.type2id[
                            self.parser.get_valid_continuating_productions(beam_hyp[0])[
                                0
                            ].type
                        ]
                    ]
                )
            if ReduceAction in valid_types:
                mask = mask + self.masks["Reduce"]
            if GenTokenAction in valid_types:
                mask = mask + self.masks["Leaves"]

        # On calcule la probabilité de chaque AST possible
        score = torch.log(logits * mask.to(self.device)) + beam_score

        # On prend les "beam size" plus grandes probas
        val, ind = score.flatten().topk(self.num_beams)
        res_score = val.unsqueeze(-1)

        # On retransforme les indices en 2-D
        ind = torch.tensor(np.unravel_index(ind.cpu().numpy(), score.shape)).T

        res_beam = (
            torch.ones((self.num_beams, decoder_inputs.size(1) + 1), dtype=int)
            * self.pad
        ).to(self.device)
        res_hyp = []

        for i, x in enumerate(ind):
            if x[1] != self.eos and x[1] != self.pad or decoder_inputs.size(1) == 1:
                res_beam[i] = torch.cat(
                    (decoder_inputs[x[0]], torch.tensor([x[1]]).to(self.device))
                )

                next_action = self.tokenizer.convert_ids_to_tokens(x[1].item())

                prod_id = re.findall(r"<NODE_\d+_(\d+)>", next_action)

                if next_action.isdecimal():
                    next_action = int(next_action)
                else:
                    try:
                        next_action = float(next_action)
                    except:
                        if next_action == "True":
                            next_action = True
                        elif next_action == "False":
                            next_action = False

                if next_action == "<REDUCE>":
                    res_hyp.append(
                        beam_hyp[x[0]].clone_and_apply_action(ReduceAction())
                    )
                elif prod_id:
                    res_hyp.append(
                        beam_hyp[x[0]].clone_and_apply_action(
                            ApplyRuleAction(self.grammar.id2prod[int(prod_id[0])])
                        )
                    )
                else:
                    res_hyp.append(
                        beam_hyp[x[0]].clone_and_apply_action(
                            GenTokenAction(next_action)
                        )
                    )
            else:
                res_beam[i] = torch.cat(
                    (decoder_inputs[x[0]], torch.tensor([x[1]]).to(self.device))
                )
                res_hyp.append(beam_hyp[x[0]])

        return res_beam, res_score, res_hyp


if __name__ == "__main__":
    import inspect
    import os
    import sys

    import astunparse
    import torch
    from asdl_pack.asdl import ASDLGrammar
    from asdl_pack.lang.py3.py3_asdl_helper import *
    from settings import (
        AST,
        DATASET,
        DEVICE,
        EMBED_LAP_DIM,
        FILE_NAME,
        GRAMMAR_PATH,
        MAX_LENGTH_OUTPUT,
        NUM_BEAMS,
        PATH_NAME,
        TOKENIZER,
    )
    from transformers import (
        BartConfig,
        BartForConditionalGeneration,
        BartTokenizer,
        PreTrainedTokenizerFast,
    )

    DATASET = "conala"
    EMBED_LAP_DIM = 64
    AST = True
    TOKENIZER = "BPE"
    PATH_NAME = "weights/bart/conala"

    grammar = ASDLGrammar.from_text(open(GRAMMAR_PATH).read())

    tokenizer = PreTrainedTokenizerFast(
        tokenizer_file=os.path.join(
            "weights",
            "tokenizer",
            f"{TOKENIZER}_{DATASET}_AST{AST}.json",
        ),
        bos_token="<SOS>",
        eos_token="<EOS>",
        pad_token="<PAD>",
    )

    """
    tokenizer = BartTokenizer.from_pretrained(
        "facebook/bart-base", local_files_only=True
    )
    tokenizer.add_tokens(
        [
            f"<NODE_{grammar.type2id[value.type]}_{key}>"
            for key, value in grammar.id2prod.items()
        ]
    )
    tokenizer.add_tokens(["<REDUCE>"])
    """

    # model = get_bartLAP2(EMBED_LAP_DIM, )
    # model.resize_token_embeddings(len(tokenizer))
    model = BartForConditionalGeneration.from_pretrained(
        "facebook/bart-base", local_files_only=True
    )
    model.resize_token_embeddings(len(tokenizer))

    model.load_state_dict(
        torch.load(
            os.path.join(
                PATH_NAME,
                "project_codegen_PRETRAINED_facebook-bart-base_PRETRAINEDTOK__LR1e-05_BS32_IPITrue_SClinear_ASTTrue_STTrue_LEAFalse.pt",
            ),
            map_location=DEVICE,
        )
    )

    data_ids = [
        "get the average of a list values for each key in dictionary `l`",
    ]
    data_encoded = tokenizer(data_ids, return_token_type_ids=False, return_tensors="pt")

    model.eval()

    generated_tokens = BeamSearch(tokenizer, grammar).generate(
        model,
        data_encoded,
        max_length=50,
        num_returned_sequence=1,
        num_beams=2,
    )

    print(
        astunparse.unparse(
            asdl_ast_to_python_ast(generated_tokens[0][2][0].tree, grammar)
        ).strip()
    )
    print("banane")
    pass
