import re

import astor
from asdl_pack.asdl import ASDLGrammar
from asdl_pack.hypothesis import Hypothesis
from asdl_pack.lang.py.py_asdl_helper import *
from asdl_pack.lang.py.py_transition_system import PythonTransitionSystem
from asdl_pack.transition_system import ApplyRuleAction, GenTokenAction, ReduceAction


def rules_to_embed():
    pass


def embed_to_rules():
    pass


def actions_from_txt(actions_file, grammar):
    file = open(actions_file).readlines()

    productions_dict = dict(
        zip(list(map(str, grammar.prod2id.keys())), grammar.prod2id.keys())
    )

    actions = []

    for action in file:
        if "Reduce" in action:
            actions.append(ReduceAction())
        elif "GenToken" in action:
            token_str = re.search(r"\[(.*)\]", action).group(1)
            if token_str is None:
                raise Exception(f"The action {action} doesn't have a proper format")
            actions.append(GenTokenAction(token_str))
        elif "ApplyRule" in action:
            production_str = re.search(r"\[(.*)\]", action).group(1)
            if production_str is None:
                raise Exception(f"The action {action} doesn't have a proper format")
            production = productions_dict.get(production_str)
            if production is None:
                raise Exception(f"The action {action} doesn't have a proper format")
            actions.append(ApplyRuleAction(production))
    return actions


if __name__ == "__main__":
    asdl_text = open("asdl_pack/lang/py3/py3_asdl.txt").read()
    grammar = ASDLGrammar.from_text(asdl_text)

    actions = actions_from_txt("res/test.txt", grammar)
