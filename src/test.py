# -----------------------------------------------------------------------------+
#                            TEST MODELS
#
# -----------------------------------------------------------------------------+

import argparse
import ast
import os
import random
import re

import astunparse
import torch
from datasets import load_metric
from tqdm import tqdm
from transformers import PreTrainedTokenizerFast

from asdl_pack.asdl import ASDLGrammar
from asdl_pack.hypothesis import Hypothesis
from asdl_pack.lang.py3.py3_asdl_helper import *
from asdl_pack.lang.py3.py3_transition_system import Python3TransitionSystem
from asdl_pack.transition_system import ApplyRuleAction, GenTokenAction, ReduceAction
from dataset.dataset import AnnotedCodeCollator, AnnotedCodeDataset
from models.transformers import Transformer
from utils.beam_search import BeamSearch

ID_MODULE = 56


def get_parser():

    parser = argparse.ArgumentParser(description="Process some integers.")

    parser.add_argument("path", help="path to the weights (weights/model/dataset/name)")
    parser.add_argument("--special", action="store_true", help="use special token")

    args = parser.parse_args()
    return args


def format_code(code, grammar):
    py_ast = ast.parse(code).body[0]
    tgt_ast = python_ast_to_asdl_ast(py_ast, grammar)
    tgt_actions, _ = transition_system.get_actions(tgt_ast)
    transition_system.current_index = 0

    # Check if the generated actions are correct
    hyp = Hypothesis()
    hyp.apply_action(ApplyRuleAction(grammar.id2prod[ID_MODULE]))

    for j, action in enumerate(tgt_actions):
        assert action.__class__ in transition_system.get_valid_continuation_types(hyp)
        if isinstance(action, ApplyRuleAction):
            assert (
                action.production
                in transition_system.get_valid_continuating_productions(hyp)
            )
        hyp.apply_action(action)

    return astunparse.unparse(asdl_ast_to_python_ast(hyp.tree, grammar)).strip()


def test(
    model,
    tokenizer,
    test_loader,
    num_beams,
    metric,
    max_length_output,
    grammar,
    ast,
    beam_search_ast,
    model_name,
    metric2=None,
    length_ast_list=None,
):
    model.eval()
    example_list = []
    score = 0
    score_compile = 0
    res_list = []
    threshold = 10
    with torch.no_grad():
        for i, batch in enumerate(tqdm(test_loader)):
            data, str_map, labels = batch

            if ast:
                generated_tokens = beam_search_ast.generate(
                    model,
                    data,
                    max_length=data["input_ids"].size(1) + max_length_output,
                    num_returned_sequence=1,
                    num_beams=num_beams,
                )

                decoded_preds = []
                for j in range(len(generated_tokens)):
                    try:
                        decoded_preds.append(
                            astunparse.unparse(
                                asdl_ast_to_python_ast(
                                    generated_tokens[j][2][0].tree, grammar
                                )
                            ).strip()
                        )
                    except:
                        decoded_preds.append("")
            else:
                generated_tokens = model.generate(
                    data["input_ids"],
                    attention_mask=data["attention_mask"],
                    max_length=min(
                        data["input_ids"].size(1) + max_length_output,
                        params.MAX_INPUT_SIZE,
                    ),
                    num_beams=num_beams,
                    eos_token_id=tokenizer.eos_token_id,
                    bos_token_id=tokenizer.bos_token_id,
                    pad_token_id=tokenizer.pad_token_id,
                )
                if model_name in ["gpt2"]:
                    generated_tokens = generated_tokens[:, data["input_ids"].size(1) :]
                decoded_preds = tokenizer.batch_decode(
                    generated_tokens, skip_special_tokens=True
                )

            decoded_labels = list(labels)
            for j in range(len(decoded_preds)):
                decoded_preds[j] = decoded_preds[j].replace("<EMPTY>", "")
                decoded_labels[j] = decoded_labels[j].replace("<EMPTY>", "")
                decoded_preds[j] = re.sub(
                    r"(:\n\s*)(el(?:if|se))", r"\1\tpass\n\2", decoded_preds[j]
                )
                for token, value in str_map[j].items():
                    decoded_preds[j] = decoded_preds[j].replace(token, value)
                    decoded_labels[j] = decoded_labels[j].replace(token, value)

                try:
                    decoded_preds[j] = format_code(decoded_preds[j], grammar)
                except:
                    pass

            decoded_preds = [pred.strip() for pred in decoded_preds]
            decoded_labels = [[label.strip()] for label in decoded_labels]

            if decoded_labels[0][0] == decoded_preds[0]:
                score += 1

            try:
                code = ""
                if re.match("(yield|return)", decoded_preds[0]):
                    code = "def dummy():\n\t" + decoded_preds[0]
                elif re.match("(break|continue)", decoded_preds[0]):
                    code = "for x in dummy:\n\t" + decoded_preds[0]
                else:
                    code = decoded_preds[0]
                compile(
                    re.sub(r"<\S{3}:\d{2}>", "unk_var", code),
                    "sumstring",
                    "exec",
                )
            except:
                pass
            else:
                score_compile += 1

            metric.add_batch(predictions=decoded_preds, references=decoded_labels)
            if metric2 is not None:
                metric2.add_batch(predictions=decoded_preds, references=decoded_labels)

                if length_ast_list[i] > threshold and length_ast_list[i] < 51:
                    res_list.append(metric2.compute()["score"])
                    threshold += 5

            if i % (len(test_loader) // 20) == 0:
                with torch.no_grad():

                    examples_in = tokenizer.batch_decode(
                        data["input_ids"], skip_special_tokens=True
                    )

                    examples = [
                        [examples_in[k], decoded_preds[k], decoded_labels[k][0]]
                        for k in range(len(examples_in))
                    ]

                    example_list.append(examples)
    try:
        res_list.append(metric2.compute()["score"])
    except:
        res_list.append(None)
    test_metric = metric.compute()
    return (
        test_metric,
        score / len(test_loader),
        score_compile / len(test_loader),
        example_list,
        res_list,
    )


if __name__ == "__main__":

    args = get_parser()

    class params:
        DEVICE = (
            torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
        )

        SPECIAL = args.special
        _, MODEL, DATASET, NAME = args.path.split("/")

        """
        test = "weigts/gpt2/django/project_codegen_PRETRAINED_gpt2_PRETRAINEDTOK_gpt2_LR1e-05_BS32_IPITrue_SClinear_ASTFalse_STFalse_LEAFalse.pt"
        _, MODEL, DATASET, NAME = test.split("/")
        SPECIAL = False
        """

        NUM_BEAMS = [4]

        MAX_LENGTH_OUTPUT = 100
        MAX_INPUT_SIZE = 1000

        AST = re.findall("_AST(.*?)_", NAME)[0] == "True"
        IGNORE_PAD_INDEX = re.findall("_IPI(.*?)_", NAME)[0] == "True"

        list_special_tokens = re.findall("_ST(.*?)_", NAME)
        USE_SPECIAL_TOKEN = (
            list_special_tokens[0] == "True" if list_special_tokens else SPECIAL
        )

        PRETRAINED_TOK = re.findall("PRETRAINEDTOK_(.*?)_", NAME)[0].replace(
            "-", "/", 1
        )

        list_pretrained = re.findall("PRETRAINED_(.*?)_", NAME)
        PRETRAINED = list_pretrained[0].replace("-", "/", 1) if list_pretrained else ""

        list_embedlap = re.findall("LAPDIM_(.*?).pt", NAME)
        EMBED_LAP_DIM = int(list_embedlap[0]) if list_embedlap else None

        BATCH_SIZE = 1
        TOKENIZER = "BPE"

        D_MODEL = int(re.findall("_DMOD(.*?)_", NAME)[0]) if not PRETRAINED else None
        N_ENC = int(re.findall("_NENC(.*?)_", NAME)[0]) if not PRETRAINED else None
        N_DEC = int(re.findall("_NDEC(.*?)_", NAME)[0]) if not PRETRAINED else None
        N_HEAD = int(re.findall("_NHEAD(.*?)_", NAME)[0]) if not PRETRAINED else None

        PATH = os.path.join("weights", MODEL, DATASET, NAME)
        GRAMMAR_PATH = "asdl_pack/lang/py3/py385_asdl.txt"

    grammar = ASDLGrammar.from_text(open(params.GRAMMAR_PATH).read())
    transition_system = Python3TransitionSystem(grammar)

    if params.DATASET == "django":
        from dataset.django import DjangoDataset

        djangoset = DjangoDataset.default_init()
        train_data, valid_data, test_data = djangoset.get_sets()

    elif params.DATASET == "django2":
        from dataset.django import DjangoDataset

        djangoset = DjangoDataset.default_init2()
        train_data, valid_data, test_data = djangoset.get_sets()

    elif params.DATASET == "django3":
        from dataset.django import DjangoDataset

        djangoset = DjangoDataset.default_init3()
        train_data, valid_data, test_data = djangoset.get_sets()

    elif params.DATASET == "conala":
        from dataset.conala import ConalaSet

        djangoset = ConalaSet.default_init()
        train_data, test_data = djangoset.get_sets()
        valid_data = test_data

    elif params.DATASET == "conala2":
        from dataset.conala import ConalaSet

        djangoset = ConalaSet.default_init2()
        train_data, test_data = djangoset.get_sets()
        valid_data = test_data

    else:
        raise NotImplementedError(f"The dataset {params.DATASET} does not exist")

    vocab = None

    if params.PRETRAINED_TOK:
        if params.MODEL == "gpt2":
            from transformers import GPT2Tokenizer

            tokenizer = GPT2Tokenizer.from_pretrained(
                params.PRETRAINED_TOK, local_files_only=True
            )  # télécharger dans un premier temps les poids, puis laisser à True pour ne pas avoir d'erreur sur les machines offline
            tokenizer.add_special_tokens({"sep_token": "<SEP>"})
            tokenizer.add_special_tokens({"pad_token": "<PAD>"})
        elif params.MODEL in ["bart", "bartLAP"]:
            from transformers import BartTokenizer

            tokenizer = BartTokenizer.from_pretrained(
                params.PRETRAINED_TOK, local_files_only=True
            )

        elif params.MODEL == "marian":
            from transformers import MarianTokenizer

            tokenizer = MarianTokenizer.from_pretrained(
                params.PRETRAINED_TOK, local_files_only=True
            )

        if params.USE_SPECIAL_TOKEN:
            if params.DATASET in [
                "django",
                "django2",
                "conala",
                "conala2",
                "conala_big",
            ]:
                tokenizer.add_tokens([f"<STR:{i:02}>" for i in range(100)])
            if params.DATASET in ["django2", "conala2"]:
                tokenizer.add_tokens([f"<VAR:{i:02}>" for i in range(12)])
            if params.AST:
                tokenizer.add_tokens(
                    [
                        f"<NODE_{grammar.type2id[value.type]}_{key}>"
                        for key, value in grammar.id2prod.items()
                    ]
                )
                tokenizer.add_tokens(["<REDUCE>"])

    else:
        if params.MODEL in [
            "default_transformer",
            "marian",
            "bart",
            "bartLAP",
            "bartLAP2",
        ]:
            tokenizer = PreTrainedTokenizerFast(
                tokenizer_file=os.path.join(
                    "weights",
                    "tokenizer",
                    f"{params.TOKENIZER}_{params.DATASET}_AST{params.AST}.json",
                ),
                bos_token="<SOS>",
                eos_token="<EOS>",
                pad_token="<PAD>",
            )
        elif params.MODEL in [
            "gpt2"
        ]:  # si on a seulement un encodeur, on utilise un séparateur
            tokenizer = PreTrainedTokenizerFast(
                tokenizer_file=os.path.join(
                    "weights",
                    "tokenizer",
                    f"{params.TOKENIZER}_{params.DATASET}_AST{params.AST}.json",
                ),
                bos_token="<SOS>",
                eos_token="<EOS>",
                pad_token="<PAD>",
                sep_token="<SEP>",
            )
        else:
            raise NotImplementedError(f"The model {params.MODEL} does not exist")

    if params.AST:
        if params.MODEL in ["bartLAP", "bartLAP2"]:
            beam_search_ast = BeamSearch(
                tokenizer,
                grammar,
                device=params.DEVICE,
                lap_pos_dim=params.EMBED_LAP_DIM,
            )
        else:
            beam_search_ast = BeamSearch(tokenizer, grammar, device=params.DEVICE)
    else:
        beam_search_ast = None

    pad_id = tokenizer.pad_token_id
    vocab_size = len(tokenizer)

    valid_data = sorted(valid_data, key=lambda x: len(x["tgt_actions"]))
    test_data = sorted(test_data, key=lambda x: len(x["tgt_actions"]))

    valid_set = AnnotedCodeDataset(valid_data, tokenizer, params, grammar=grammar)
    test_set = AnnotedCodeDataset(test_data, tokenizer, params, grammar=grammar)

    valid_ordered_tgt_actions = list(map(lambda x: len(x["tgt_actions"]), valid_data))
    test_ordered_tgt_actions = list(map(lambda x: len(x["tgt_actions"]), test_data))

    if params.MODEL in [
        "default_transformer",
        "marian",
        "bart",
    ]:  # si le modèle a un décodeur
        collate_fn = AnnotedCodeCollator(params, pad_id=pad_id, de_pad_id=pad_id)
        collate_fn_valid = AnnotedCodeCollator(
            params, pad_id=pad_id, de_pad_id=pad_id, valid=True
        )
    elif params.MODEL in ["bartLAP", "bartLAP2"]:
        collate_fn = AnnotedCodeCollator(
            params, pad_id=pad_id, de_pad_id=pad_id, lap_pos_dim=params.EMBED_LAP_DIM
        )
        collate_fn_valid = AnnotedCodeCollator(
            params,
            pad_id=pad_id,
            de_pad_id=pad_id,
            valid=True,
            lap_pos_dim=params.EMBED_LAP_DIM,
        )
    elif params.MODEL in ["gpt2"]:
        collate_fn = AnnotedCodeCollator(
            params, pad_id=pad_id, sep_id=tokenizer.sep_token_id
        )
        collate_fn_valid = AnnotedCodeCollator(
            params, pad_id=pad_id, sep_id=tokenizer.sep_token_id, valid=True
        )
    else:
        raise NotImplementedError(f"The model {params.MODEL} does not exist")

    valid_loader = torch.utils.data.DataLoader(
        dataset=valid_set,
        batch_size=params.BATCH_SIZE,
        shuffle=False,
        collate_fn=collate_fn_valid,
    )

    test_loader = torch.utils.data.DataLoader(
        dataset=test_set,
        batch_size=params.BATCH_SIZE,
        shuffle=False,
        collate_fn=collate_fn_valid,
    )

    if params.MODEL in ["default_transformer"]:  # custom models
        model = Transformer(
            vocab_size=vocab_size,
            prod_size=vocab_size,
            d_model=params.D_MODEL,
            N_en=params.N_ENC,
            N_de=params.N_DEC,
            nhead=params.N_HEAD,
        )

    # Huggingface models
    elif params.MODEL == "gpt2":  # huggingface models
        from transformers import GPT2Config, GPT2LMHeadModel

        if params.PRETRAINED:

            model = GPT2LMHeadModel.from_pretrained(
                params.PRETRAINED, local_files_only=True
            )  # télécharger dans un premier temps les poids, puis laisser à True pour ne pas avoir d'erreur sur les machines offline
            model.resize_token_embeddings(vocab_size)
        else:
            config = GPT2Config(
                vocab_size=vocab_size,
                n_positions=params.MAX_INPUT_SIZE,
                n_ctx=params.MAX_INPUT_SIZE,
                n_embd=params.D_MODEL,
                n_layer=params.N_ENC,
                n_head=params.N_HEAD,
            )
            model = GPT2LMHeadModel(config)

    elif params.MODEL == "bart":
        from transformers import BartConfig, BartForConditionalGeneration

        if params.PRETRAINED:
            model = BartForConditionalGeneration.from_pretrained(
                params.PRETRAINED, local_files_only=True
            )
            model.resize_token_embeddings(vocab_size)
        else:
            config = BartConfig(
                vocab_size=vocab_size,
                d_model=params.D_MODEL,
                encoder_layers=params.N_ENC,
                decoder_layers=params.N_DEC,
                encoder_attention_heads=params.N_HEAD,
                decoder_attention_head=params.N_HEAD,
                max_position_embeddings=params.MAX_INPUT_SIZE,
            )
            model = BartForConditionalGeneration(config)

    elif params.MODEL == "bartLAP":
        from transformers import BartConfig, BartForConditionalGeneration

        from models.bartLAP import get_bartLAP

        if params.PRETRAINED:
            model = get_bartLAP(params.EMBED_LAP_DIM, params.PRETRAINED)
            model.resize_token_embeddings(vocab_size)
        else:
            config = BartConfig(
                vocab_size=vocab_size,
                d_model=params.D_MODEL,
                encoder_layers=params.N_ENC,
                decoder_layers=params.N_DEC,
                encoder_attention_heads=params.N_HEAD,
                decoder_attention_head=params.N_HEAD,
                max_position_embeddings=params.MAX_INPUT_SIZE,
            )
            model = get_bartLAP(params.EMBED_LAP_DIM, config=config)
            model.resize_token_embeddings(vocab_size)

    elif params.MODEL == "bartLAP2":
        from transformers import BartConfig, BartForConditionalGeneration

        from models.bartLAP2 import get_bartLAP2

        if params.PRETRAINED:
            model = get_bartLAP2(params.EMBED_LAP_DIM, params.PRETRAINED)
            model.resize_token_embeddings(vocab_size)
        else:
            config = BartConfig(
                vocab_size=vocab_size,
                d_model=params.D_MODEL,
                encoder_layers=params.N_ENC,
                decoder_layers=params.N_DEC,
                encoder_attention_heads=params.N_HEAD,
                decoder_attention_head=params.N_HEAD,
                max_position_embeddings=params.MAX_INPUT_SIZE,
            )
            model = get_bartLAP2(params.EMBED_LAP_DIM, config=config)
            model.resize_token_embeddings(vocab_size)

    elif params.MODEL == "marian":
        from transformers import MarianConfig, MarianMTModel

        if params.PRETRAINED:

            model = MarianMTModel.from_pretrained(
                params.PRETRAINED, local_files_only=True
            )
            model.resize_token_embeddings(vocab_size)
        else:
            config = MarianConfig(
                vocab_size=vocab_size,
                d_model=params.D_MODEL,
                encoder_layers=params.N_ENC,
                decoder_layers=params.N_DEC,
                encoder_attention_heads=params.N_HEAD,
                decoder_attention_head=params.N_HEAD,
                max_position_embeddings=params.MAX_INPUT_SIZE,
            )
            model = MarianMTModel(config)
    else:
        raise NotImplementedError(f"The model {params.MODEL} does not exist")

    print(
        f"Trainable parameters : {sum(p.numel() for p in model.parameters() if p.requires_grad)}"
    )
    metric = load_metric("sacrebleu")

    if params.AST:
        metric2 = load_metric("sacrebleu")
    else:
        metric2 = None

    model.load_state_dict(
        torch.load(
            params.PATH,
            map_location=params.DEVICE,
        )
    )

    model.to(params.DEVICE)

    results_valid = []
    results_test = []

    for beams in params.NUM_BEAMS:
        print(f"Beam size : {beams}")
        bleu_metric, acc, compile_acc, example_list, res_list = test(
            model,
            tokenizer,
            valid_loader,
            beams,
            metric,
            params.MAX_LENGTH_OUTPUT,
            grammar,
            params.AST,
            beam_search_ast,
            params.MODEL,
            metric2=metric2,
            length_ast_list=valid_ordered_tgt_actions,
        )
        results_valid.append([bleu_metric, acc, compile_acc, res_list])
        print(
            f"BLEU : {bleu_metric}, Accuracy : {acc}, Compilation Accuracy : {compile_acc}, BLEU_LEN : {res_list}"
        )
        print("-------------------------")
        for example in example_list:
            print(f"{example}\n")
            print("---")

        bleu_metric, acc, compile_acc, example_list, res_list = test(
            model,
            tokenizer,
            test_loader,
            beams,
            metric,
            params.MAX_LENGTH_OUTPUT,
            grammar,
            params.AST,
            beam_search_ast,
            params.MODEL,
            metric2=metric2,
            length_ast_list=test_ordered_tgt_actions,
        )
        results_test.append([bleu_metric, acc, compile_acc, res_list])
        print(
            f"BLEU : {bleu_metric}, Accuracy : {acc}, Compilation Accuracy : {compile_acc}, BLEU_LEN : {res_list}"
        )
        print("-------------------------")
        for example in example_list:
            print(f"{example}\n")
            print("---")

    print("#-----------------------------------------------------------#")
    print(f"MODEL : {params.MODEL}")
    print(f"DATASET : {params.DATASET}")
    print(f"NAME : {params.NAME}")

    print("Validation")
    print(f"{'':<10} {'BLEU':<10} {'ACC':<10} {'COMP':<10} {'LEN_RES':<10}")
    for i, (bleu, accu, compile_accu, res_list) in enumerate(results_valid):
        print(
            f"{params.NUM_BEAMS[i]:<10} {bleu['score']:<10} {accu:<10} {compile_accu:<10} {str(res_list):<10}"
        )

    print("Test")
    print(f"{'':<10} {'BLEU':<10} {'ACC':<10} {'COMP':<10} {'LEN_RES':<10}")
    for i, (bleu, accu, compile_accu, res_list) in enumerate(results_test):
        print(
            f"{params.NUM_BEAMS[i]:<10} {bleu['score']:<10} {accu:<10} {compile_accu:<10} {str(res_list):<10}"
        )
