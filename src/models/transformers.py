import copy
import math

import torch
import torch.nn.functional as F
from asdl_pack.transition_system import (
    ApplyRuleAction,
    GenTokenAction,
    PadAction,
    ReduceAction,
)
from torch import nn


class Embedder(nn.Module):
    def __init__(self, vocab_size, embedding_size):
        super().__init__()
        self.embedder = nn.Embedding(vocab_size, embedding_size)

    def forward(self, x):
        return self.embedder(x)


class PositionalEncoding(nn.Module):
    def __init__(self, d_model, dropout=0.1, max_len=5000):
        super().__init__()
        self.dropout = nn.Dropout(p=dropout)

        pe = torch.zeros(max_len, d_model)
        position = torch.arange(0, max_len, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp(
            torch.arange(0, d_model, 2).float() * (-math.log(10000.0) / d_model)
        )
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0).transpose(0, 1)
        self.register_buffer("pe", pe)

    def forward(self, x):
        x = x + self.pe[: x.size(0), :]
        return self.dropout(x)


class Encoder(nn.Module):
    def __init__(self, vocab_size, d_model, N, nhead):
        super().__init__()
        self.N = N
        self.embedder = Embedder(vocab_size, d_model)
        self.pe = PositionalEncoding(d_model)
        self.norm = nn.LayerNorm(d_model)
        self.encoder_layer = nn.TransformerEncoderLayer(d_model, nhead)
        self.layers = nn.ModuleList(
            [copy.deepcopy(self.encoder_layer) for i in range(N)]
        )

    def forward(self, src, src_mask=None, padding_mask=None):
        x = self.embedder(src)
        x = self.pe(x)
        for i in range(self.N):
            x = self.layers[i](x, src_mask=src_mask, src_key_padding_mask=padding_mask)
        return self.norm(x)


class Decoder(nn.Module):
    def __init__(self, prod_size, d_model, N, nhead):
        """Decoder of the model, takes a rules sequence in entrance,
        and returns an action

        Args:
            prod_size (int): [description]
            d_model (int): [description]
            N (int): [description]
            nhead (int): [description]
        """

        super().__init__()
        self.N = N
        self.embedder = Embedder(prod_size, d_model)
        self.pe = PositionalEncoding(d_model)
        self.norm = nn.LayerNorm(d_model)
        self.decoder_layer = nn.TransformerDecoderLayer(d_model, nhead)
        self.layers = nn.ModuleList(
            [copy.deepcopy(self.decoder_layer) for i in range(N)]
        )

    def forward(
        self, rules_seq, e_outputs, tgt_mask=None, padding_mask=None, memory_mask=None
    ):

        x = self.embedder(rules_seq)
        x = self.pe(x)
        for i in range(self.N):
            x = self.layers[i](
                x,
                memory=e_outputs,
                tgt_mask=tgt_mask,
                tgt_key_padding_mask=padding_mask,
                memory_key_padding_mask=memory_mask,
            )
        return self.norm(x)


class OutputLayer(nn.Module):
    def __init__(self, d_model, prod_size):
        """Final layer of the network

        Args:
            d_model (int): size of the output of the encoder and the decoder
            prod_size (int): size of output (len(grammar) + 1 + len(primitive_vocab))
        """
        super().__init__()
        self.layer = nn.Linear(d_model, prod_size)

    def forward(self, x):
        x = self.layer(x)
        return F.softmax(x, dim=-1)


class Transformer(nn.Module):
    def __init__(self, vocab_size, prod_size, d_model, N_en, N_de, nhead):
        """THE Transformer :)

        Args:
            vocab_size (int): vocab size in the input of the encoder
            prod_size (int): size of output (len(grammar) + 1 + len(primitive_vocab))
            d_model (int): size of the output of the encoder and the decoder
            N_en (int): how much encoder layer we have
            N_de (int): how much decoder layer we have
            nhead (int): number of multi-headed attention modules
        """
        super().__init__()
        self.encoder = Encoder(vocab_size, d_model, N_en, nhead)
        self.decoder = Decoder(prod_size, d_model, N_de, nhead)
        self.out = OutputLayer(d_model, prod_size)

    def forward(
        self,
        en_in,
        de_in,
        labels,
        en_pad_mask=None,
        tgt_mask=None,
        attention_mask=None,
        memory_mask=None,
    ):
        """

        Args:
            en_in (tensor): input of the encoder
            de_in (tensor): input / output of the decoder
            labels (tensor)
            src_mask (tensor): mask of the encoder
            tgt_mask (tensor): mask of the decoder
            production_type (int): id of the rule type (index of the list in
                                     self.productions)
        """
        e_outputs = self.encoder(
            en_in,
            padding_mask=en_pad_mask,
        )
        d_output = self.decoder(
            de_in,
            e_outputs,
            tgt_mask=tgt_mask,
            padding_mask=attention_mask,
            memory_mask=memory_mask,
        )
        return self.out(d_output)
