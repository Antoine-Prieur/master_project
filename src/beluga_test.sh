#!/bin/bash
#SBATCH --job-name=train_astgen
#SBATCH --account=def-mparizea
#SBATCH -t 0-01:00
#SBATCH --nodes=1
#SBATCH --gres=gpu:1
#SBATCH --mem=4G
#SBATCH --output=out/slurm-%A_%a.out

module load python/3.8.2 gcc/9.3 arrow/4.0.0

source ~/ENV/bin/activate

python -u test.py
