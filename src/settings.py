import os
import sys
from pathlib import Path

from torch import cuda, device

import wandb
from typings import DatasetType, ModelType, PretrainedType, SchedulerType, TokenizerType

python_version = sys.version_info

assert python_version[0] == 3, "Python doit être au moins en 3.6"
assert python_version[1] > 5, "Python doit être au moins en 3.6"

os.environ["WANDB_MODE"] = "dryrun"
wandb.init(project="project_codegen", entity="antoine-prieur")

DEVICE: device = device("cuda") if cuda.is_available() else device("cpu")

# wandb settings


LEARNING_RATE: float = wandb.config.learning_rate
EPOCHS: int = wandb.config.epochs
BATCH_SIZE: int = wandb.config.batch_size
KEEP_WEIGHTS: bool = wandb.config.keep_weights
MODEL: ModelType = wandb.config.model
GRAD_ACCUM_STEPS: int = wandb.config.grad_accum_steps
TRAINING_MODE: int = wandb.config.training_mode
PRETRAINED: PretrainedType = wandb.config.pretrained
PRETRAINED_TOK: PretrainedType = wandb.config.pretrained_tok
DATASET: DatasetType = wandb.config.dataset
MAX_INPUT_SIZE: int = wandb.config.max_input_size
SOFTMAX: bool = wandb.config.softmax
IGNORE_PAD_INDEX: bool = wandb.config.ignore_pad_index
SCHEDULER: SchedulerType = wandb.config.scheduler
AST: bool = wandb.config.ast
LEAVES: bool = wandb.config.leaves
D_MODEL: int = wandb.config.d_model
N_HEAD: int = wandb.config.n_head
N_ENC: int = wandb.config.n_enc
N_DEC: int = wandb.config.n_dec
CHEATER: bool = wandb.config.cheater
TOKENIZER: TokenizerType = wandb.config.tokenizer
NUM_BEAMS: int = wandb.config.num_beams
MAX_LENGTH_OUTPUT: int = wandb.config.max_length_output
EMBED_LAP_DIM: int = wandb.config.embed_lap_dim
USE_SPECIAL_TOKEN: bool = wandb.config.use_special_token

if wandb.config.debug:
    print(
        r"/!\ YOU ARE IN DEBUG MODE, TURN IT OFF IN CONFIG IN SETTINGS IF YOU ARE NOT DEBUGGING /!\ "
    )
    LEARNING_RATE = 0.01  # type: ignore
    EPOCHS = 2  # type: ignore
    BATCH_SIZE = 2  # type: ignore
    KEEP_WEIGHTS = False  # type: ignore

# other settings
PATH_NAME = Path("weights", MODEL, DATASET)
if PRETRAINED:
    file_name = f"project_codegen_PRETRAINED_{PRETRAINED.replace('/', '-')}_PRETRAINEDTOK_{PRETRAINED_TOK.replace('/', '-')}_LR{LEARNING_RATE}_BS{BATCH_SIZE*GRAD_ACCUM_STEPS}_IPI{IGNORE_PAD_INDEX}_SC{SCHEDULER}_AST{AST}_ST{USE_SPECIAL_TOKEN}_LEA{LEAVES}"
else:
    file_name = f"project_codegen_LR{LEARNING_RATE}_BS{BATCH_SIZE*GRAD_ACCUM_STEPS}_PRETRAINEDTOK_{PRETRAINED_TOK.replace('/', '-')}_MIS{MAX_INPUT_SIZE}_SM{SOFTMAX}_IPI{IGNORE_PAD_INDEX}_SC{SCHEDULER}_AST{AST}_LEA{LEAVES}_DMOD{D_MODEL}_NHEAD{N_HEAD}_NENC{N_ENC}_NDEC{N_DEC}_TOK{TOKENIZER}_ST{USE_SPECIAL_TOKEN}_CHEATER{CHEATER}"

if MODEL in ["bartLAP", "bartLAP2"]:
    file_name += f"_LAPDIM_{EMBED_LAP_DIM}"

FILE_NAME = file_name
PATH_NAME.mkdir(parents=True, exist_ok=True)

GRAMMAR_PATH = "asdl_pack/lang/py3/py385_asdl.txt"

print(f"Running on {DEVICE}")
