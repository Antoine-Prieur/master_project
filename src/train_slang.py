import json
import math
import os
import random
import time
from typing import Any

import torch
from augly.text import SimulateTypos, SplitWords
from datasets import load_metric
from torch.utils.data import Dataset, random_split
from tqdm import tqdm
from transformers import (
    AutoTokenizer,
    BartForConditionalGeneration,
    BatchEncoding,
    PreTrainedTokenizerBase,
)
from transformers.data.data_collator import AnnotedCodeCollatorForSeq2Seq

import wandb
from settings import (
    BATCH_SIZE,
    DATASET,
    DEVICE,
    EPOCHS,
    FILE_NAME,
    GRAD_ACCUM_STEPS,
    LEARNING_RATE,
    MAX_INPUT_SIZE,
    MAX_LENGTH_OUTPUT,
    N_DEC,
    NUM_BEAMS,
    PATH_NAME,
    PRETRAINED,
    PRETRAINED_TOK,
    SCHEDULER,
    TOKENIZER,
    TRAINING_MODE,
)

# -----------------------------------------------------------------------------+
#
#
# -----------------------------------------------------------------------------+


assert DATASET == "pierre"

wandb.init(project="code-gen")


def slangify(text, missspelled_dict_path):
    """Slangify a text by inserting spaces, creating typos and replacing words with slang

    Parameters
    ----------
    text : str
        The text to slangify
    p : float
        The probability to slangify (higher = more slang)

    Returns
    -------
    str :
        The slangified text
    """
    # nb_words = len(text.split())

    split_maker = SplitWords(
        aug_word_p=0.1,
        min_char=5,
        aug_word_min=0,
        aug_word_max=1000,
        priority_words=None,
        n=1,  # Return only one augmented string
        p=1,  # Apply everytime
    )

    typo_maker = SimulateTypos(
        aug_char_p=0.3,
        aug_word_p=0.3,
        min_char=2,
        aug_char_min=1,
        aug_char_max=2,
        aug_word_min=1,
        aug_word_max=1000,
        n=1,  # Return only one augmented string
        misspelling_dict_path=missspelled_dict_path,
        priority_words=None,
        p=1.0,  # Apply everytime
    )

    text = split_maker([text])[0]
    text = typo_maker([text])[0]

    return text


class SpellDataset:
    def __init__(
        self,
        tokenizer,
        dataset_paths,
        slang_dict_path,
        augment=True,
    ):

        self.should_augment = augment
        self.tokenizer = tokenizer
        self.data = []
        for path in dataset_paths:
            with open(path, "r") as file:
                data = json.load(file)
                self.data.extend(data["samples"])

        self.slangs = {}
        for path in slang_dict_path:
            with open(path, "r") as file:
                data = json.load(file)
                for real_word, slangs in data.items():
                    sl = set(self.slangs.get(real_word, []))
                    sl.update(slangs)
                    self.slangs[real_word] = list(sl)

        self.slang_path = "./data/pierre/temp_slang_file.json"
        with open(self.slang_path, "w") as slang_file:
            slang_file.write(json.dumps(self.slangs, indent=2, ensure_ascii=False))

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        slang_text = self.data[idx]["text"]
        clean_text = self.data[idx]["label"]

        if self.should_augment and random.random() > 0.5:
            slang_text = slangify(clean_text, self.slang_path)

        slang_toks: BatchEncoding = self.tokenizer(slang_text)
        clean_toks: BatchEncoding = self.tokenizer(clean_text)

        return {
            "input_ids": slang_toks["input_ids"],
            "attention_mask": slang_toks["attention_mask"],
            "labels": clean_toks["input_ids"],
        }

    def __del__(self):
        os.remove(self.slang_path)


tokenizer = AutoTokenizer.from_pretrained(PRETRAINED_TOK)
pad_id = tokenizer.pad_token_id
vocab_size = len(tokenizer)

dataset = SpellDataset(
    tokenizer,
    dataset_paths=["./data/pierre/texto4science.json", "./data/pierre/Covid.json"],
    slang_dict_path=[
        "./data/pierre/texto4science_dict.json",
        "./data/pierre/Covid_dict.json",
    ],
    augment=True,
)

train_size = math.floor(0.8 * len(dataset))
test_size = len(dataset) - train_size
train_set, valid_set = random_split(dataset, [train_size, test_size])

model = BartForConditionalGeneration.from_pretrained(PRETRAINED)
model.resize_token_embeddings(vocab_size)

collate_fn = AnnotedCodeCollatorForSeq2Seq(
    tokenizer,
    model=model,
)

train_loader = torch.utils.data.DataLoader(
    dataset=train_set, batch_size=BATCH_SIZE, shuffle=True, collate_fn=collate_fn
)
valid_loader = torch.utils.data.DataLoader(
    dataset=valid_set, batch_size=BATCH_SIZE, shuffle=True, collate_fn=collate_fn
)

print(
    f"Trainable parameters : {sum(p.numel() for p in model.parameters() if p.requires_grad)}"
)
metric = load_metric("sacrebleu")

model.to(DEVICE)
wandb.watch(model)

optimizer = torch.optim.AdamW(model.parameters(), lr=LEARNING_RATE)

if SCHEDULER == "constant":
    from transformers import get_constant_schedule_with_warmup

    scheduler = get_constant_schedule_with_warmup(
        optimizer=optimizer, num_warmup_steps=500
    )
elif SCHEDULER == "linear":
    from transformers import get_linear_schedule_with_warmup

    scheduler = get_linear_schedule_with_warmup(
        optimizer=optimizer,
        num_warmup_steps=500,
        num_training_steps=EPOCHS * math.ceil(len(train_loader) / GRAD_ACCUM_STEPS),
    )

elif SCHEDULER == "cosine":
    from transformers import get_cosine_schedule_with_warmup

    scheduler = get_cosine_schedule_with_warmup(
        optimizer=optimizer,
        num_warmup_steps=500,
        num_training_steps=EPOCHS * math.ceil(len(train_loader) / GRAD_ACCUM_STEPS),
    )

elif SCHEDULER == "hardcosine":
    from transformers import get_cosine_with_hard_restarts_schedule_with_warmup

    scheduler = get_cosine_with_hard_restarts_schedule_with_warmup(
        optimizer=optimizer,
        num_warmup_steps=500,
        num_training_steps=EPOCHS * math.ceil(len(train_loader) / GRAD_ACCUM_STEPS),
    )

else:
    raise NotImplementedError(f"The scheduler {SCHEDULER} does not exist")


def train():
    model.train()
    acc = length = 0
    for i, batch in enumerate(tqdm(train_loader)):

        batch["input_ids"] = batch["input_ids"].to(DEVICE)
        batch["attention_mask"] = batch["attention_mask"].to(DEVICE)
        batch["labels"] = batch["labels"].to(DEVICE)
        batch["decoder_input_ids"] = batch["decoder_input_ids"].to(DEVICE)

        output = model(**batch)
        loss = output.loss
        loss = loss / GRAD_ACCUM_STEPS

        loss.backward()

        if i % GRAD_ACCUM_STEPS == 0 or i == len(train_loader) - 1:

            optimizer.step()
            scheduler.step()
            optimizer.zero_grad()

            wandb.log(
                {"LR": optimizer.param_groups[0]["lr"], "train/loss": loss.item()}
            )

        if i == 0:
            with torch.no_grad():

                examples_in = tokenizer.batch_decode(batch["input_ids"])
                examples_lbl = tokenizer.batch_decode(
                    batch["labels"].masked_fill(batch["labels"] == -100, pad_id)
                )
                examples_out = tokenizer.batch_decode(output.logits.argmax(-1))
                examples = [
                    [examples_in[i], examples_out[i], examples_lbl[i]]
                    for i in range(len(examples_in))
                ]

                wandb.log(
                    {
                        "train/examples": wandb.Table(
                            data=examples,
                            columns=["Description", "Code généré", "Code attendu"],
                        ),
                    }
                )


def evaluate():
    model.eval()
    with torch.no_grad():
        for i, batch in enumerate(tqdm(valid_loader)):

            batch["input_ids"] = batch["input_ids"].to(DEVICE)
            batch["attention_mask"] = batch["attention_mask"].to(DEVICE)
            batch["labels"] = batch["labels"].to(DEVICE)
            batch["decoder_input_ids"] = batch["decoder_input_ids"].to(DEVICE)

            generated_tokens = model.generate(
                batch["input_ids"],
                attention_mask=batch["attention_mask"],
                max_length=MAX_LENGTH_OUTPUT,
                num_beams=NUM_BEAMS,
                eos_token_id=tokenizer.eos_token_id,
                bos_token_id=tokenizer.bos_token_id,
                pad_token_id=tokenizer.pad_token_id,
            )

            decoded_preds = tokenizer.batch_decode(
                generated_tokens, skip_special_tokens=True
            )
            decoded_labels = tokenizer.batch_decode(
                batch["labels"].masked_fill(batch["labels"] == -100, pad_id),
                skip_special_tokens=True,
            )

            decoded_preds = [pred.strip() for pred in decoded_preds]
            decoded_labels = [[label.strip()] for label in decoded_labels]

            metric.add_batch(predictions=decoded_preds, references=decoded_labels)

            if i == len(valid_loader) - 1:
                with torch.no_grad():

                    examples_in = tokenizer.batch_decode(
                        batch["input_ids"], skip_special_tokens=True
                    )

                    examples = [
                        [examples_in[i], decoded_preds[i], decoded_labels[i][0]]
                        for i in range(len(examples_in))
                    ]

                    wandb.log(
                        {
                            "valid/examples": wandb.Table(
                                data=examples,
                                columns=["Description", "Code généré", "Code attendu"],
                            ),
                        }
                    )
    valid_metric = metric.compute()
    return valid_metric


if __name__ == "__main__":
    best_score = float("-inf")

    for i in range(EPOCHS):

        start_time = time.time()
        train()
        valid_metric = evaluate()

        if valid_metric["score"] > best_score:
            best_score = valid_metric["score"]
            torch.save(
                model.state_dict(),
                os.path.join(PATH_NAME, f"{FILE_NAME}.pt"),
            )

        wandb.log({"valid/BLEU": valid_metric["score"]})

        print(
            f" {i}/{EPOCHS} | BLEU score valid : {valid_metric['score']:5.2f} | epoch time : {time.time() - start_time:2.2f}s | learning rate : {optimizer.param_groups[0]['lr']}"
        )
