#!/bin/bash
#SBATCH --job-name=train_astgen
#SBATCH --account=def-mparizea
#SBATCH -t 0-10:00
#SBATCH --nodes=2
#SBATCH --gres=gpu:k80:4
#SBATCH -o ~/out/00train.out
#SBATCH -e ~/out/00train.err

module load python/3.8.2

cd "${$SLURM_SUBMIT_DIR}"

# echo "" > ~/out/00train.out
# echo "" > ~/out/00train.err

source ~/env/bin/activate

python train.py
