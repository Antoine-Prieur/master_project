from asdl_pack.asdl import ASDLGrammar
from asdl_pack.hypothesis import *

python_version = sys.version_info

if python_version[0] == 3:
    from asdl_pack.lang.py3.py3_transition_system import (
        Python3TransitionSystem as PythonTransitionSystem,
    )

    if python_version[1] >= 6:
        from asdl_pack.lang.py3.py3_asdl_helper import *

        grammar_path = "asdl_pack/lang/py3/py385_asdl.txt"
    else:
        from asdl_pack.lang.py.py_asdl_helper import *

        grammar_path = "asdl_pack/lang/py3/py3_asdl.txt"
else:
    from asdl_pack.lang.py.py_asdl_helper import *
    from asdl_pack.lang.py.py_transition_system import (
        PythonTransitionSystem as PythonTransitionSystem,
    )

    grammar_path = "asdl_pack/lang/py3/py_asdl.txt"

asdl_text = open(grammar_path).read()
grammar = ASDLGrammar.from_text(asdl_text)

rules = []
parent_node = ASDLCompositeType("mod")

while True:
    print(len(grammar._productions))
    productions = grammar._productions[parent_node]
    print(productions)
    print("Choisissez le noeud que vous souhaitez :")
    for i, production in enumerate(productions):
        print(f"{i} : {production}")
    choice = input("Noeud choisi : ")
    rules.append(productions[choice])
    break


def predict_node(production, cardinality):
    pass
