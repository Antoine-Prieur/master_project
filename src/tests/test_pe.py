import settings as params
import torch
from asdl_pack.asdl import ASDLGrammar
from dataset.dataset import AnnotedCodeCollator, AnnotedCodeDataset
from dataset.django import DjangoDataset
from settings import BATCH_SIZE, GRAMMAR_PATH
from transformers import BartForConditionalGeneration, BartTokenizer

djangoset = DjangoDataset.default_init()

train_data, valid_data, test_data = djangoset.get_sets()

grammar = ASDLGrammar.from_text(open(GRAMMAR_PATH).read())
tokenizer = BartTokenizer.from_pretrained("facebook/bart-base", local_files_only=True)

train_set = AnnotedCodeDataset(train_data, tokenizer, params, grammar=grammar)

collate_fn = AnnotedCodeCollator(
    params, pad_id=tokenizer.pad_token_id, de_pad_id=tokenizer.pad_token_id
)

loader = torch.utils.data.DataLoader(
    dataset=train_set, batch_size=BATCH_SIZE, shuffle=True, collate_fn=collate_fn
)


model = BartForConditionalGeneration.from_pretrained(
    "facebook/bart-base", local_files_only=True
)
model2 = BartForConditionalGeneration.from_pretrained(
    "facebook/bart-base", local_files_only=True
)

for batch in loader:
    data, _, labels = batch

    y1 = model(**data)
    y2 = model(**data)

    pass
