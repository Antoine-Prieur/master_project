import ast
import sys

import astunparse
from asdl_pack.asdl import ASDLGrammar
from asdl_pack.hypothesis import *
from utils.gft import edge_index_to_eigenvectors, remove_root_index

python_version = sys.version_info
print(python_version)
if python_version[0] == 3:
    from asdl_pack.lang.py3.py3_transition_system import (
        Python3TransitionSystem as PythonTransitionSystem,
    )

    if python_version[1] >= 6:
        from asdl_pack.lang.py3.py3_asdl_helper import *

        grammar_path = "asdl_pack/lang/py3/py385_asdl.txt"
    else:
        from asdl_pack.lang.py.py_asdl_helper import *

        grammar_path = "asdl_pack/lang/py3/py3_asdl.txt"
else:
    from asdl_pack.lang.py.py_asdl_helper import *
    from asdl_pack.lang.py.py_transition_system import (
        PythonTransitionSystem as PythonTransitionSystem,
    )

    grammar_path = "asdl_pack/lang/py/py_asdl.txt"

asdl_text = open(grammar_path).read()
grammar = ASDLGrammar.from_text(asdl_text)

py_code = open("data/ast_example.py").read()
py_ast = ast.parse(py_code)

for node in ast.walk(py_ast):
    print(node)

asdl_ast = python_ast_to_asdl_ast(py_ast.body[0], grammar)


parser = PythonTransitionSystem(grammar)
actions, x = parser.get_actions(asdl_ast)

hypothesis = Hypothesis()
hypothesis.apply_action(ApplyRuleAction(grammar.id2prod[56]))
for t, action in enumerate(actions, 1):
    # the type of the action should belong to one of the valid continuing types
    # of the transition system
    print(action)
    assert action.__class__ in parser.get_valid_continuation_types(hypothesis)
    # Prochaine production
    # print(parser.get_valid_continuating_productions(hypothesis))

    # if it's an ApplyRule action, the production rule should belong to the
    # set of rules with the same LHS type as the current rule
    if isinstance(action, ApplyRuleAction) and hypothesis.frontier_node:
        assert action.production in grammar[hypothesis.frontier_field.type]

    hypothesis.apply_action(action)


hypothesis.apply_action(ReduceAction())
test = asdl_ast_to_python_ast(hypothesis.tree, grammar)
test2 = astunparse.unparse(test)
src3 = test2.strip()

parser.current_index = 0
edge_index = remove_root_index(hypothesis.edge_index)
edge_index_to_eigenvectors(hypothesis.edge_index)

print(src3)
pass
