import os
import time


def temps_exécution(fonction):
    def nvl_fct(*args, **kwargs):
        x = time.time()
        resultat = fonction(*args, **kwargs)
        print(time.time() - x)
        return resultat

    return nvl_fct


def fonction(arg1: int):
    return arg1


print(temps_exécution(fonction))


print(2)
