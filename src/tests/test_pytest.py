from torch import Tensor


def test_beam():
    from utils.mask import generate_square_subsequent_mask

    res = generate_square_subsequent_mask(10)
    assert isinstance(res, Tensor)
    assert res.shape == (10, 10)
