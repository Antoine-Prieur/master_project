import torch
from transformers import BartTokenizer, BartForConditionalGeneration

tokenizer = BartTokenizer.from_pretrained("facebook/bart-base", local_files_only=True)

model = BartForConditionalGeneration.from_pretrained(
    "facebook/bart-base", local_files_only=True
)
model.load_state_dict(
    torch.load(
        "./weights/bart/pierre/project_codegen_PRETRAINED_facebook-bart-base_PRETRAINEDTOK_facebook-bart-base_LR1e-05_BS4_IPITrue_SClinear_ASTFalse_LEAFalse.pt",
        map_location=torch.device("cpu"),
    )
)


while (sentence := input(">>")) != "exit":
    data_encoded = tokenizer(sentence, return_token_type_ids=False, return_tensors="pt")

    generated_tokens = model.generate(
        **data_encoded,
        max_length=128,
        num_beams=4,
        eos_token_id=tokenizer.eos_token_id,
        bos_token_id=tokenizer.bos_token_id,
        pad_token_id=tokenizer.pad_token_id,
    )

    print(tokenizer.batch_decode(generated_tokens, skip_special_tokens=True)[0])
