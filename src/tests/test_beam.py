from models.bartLAP import get_bartLAP
from settings import (
    AST,
    DATASET,
    DEVICE,
    EMBED_LAP_DIM,
    FILE_NAME,
    MAX_LENGTH_OUTPUT,
    NUM_BEAMS,
    PATH_NAME,
    PRETRAINED,
    TOKENIZER,
)
from transformers import (
    BartConfig,
    BartForConditionalGeneration,
    BartTokenizer,
    PreTrainedTokenizerFast,
)

tokenizer = PreTrainedTokenizerFast(
    tokenizer_file=os.path.join(
        "weights",
        "tokenizer",
        f"{TOKENIZER}_{DATASET}_AST{AST}.json",
    ),
    bos_token="<SOS>",
    eos_token="<EOS>",
    pad_token="<PAD>",
)

model = get_bartLAP(EMBED_LAP_DIM, PRETRAINED)

model.resize_token_embeddings(len(tokenizer))


# model.load_state_dict(
#     torch.load(
#         os.path.join(
#             PATH_NAME,
#             "project_codegen_PRETRAINED_facebook-bart-base_PRETRAINEDTOK__LR1e-05_BS4_IPITrue_SClinear_ASTTrue_LEAFalse.pt",
#         ),
#         map_location=DEVICE,
#     )
# )

data_ids = [
    "if cuisine is equal to 7",
]
data_encoded = tokenizer(data_ids, return_token_type_ids=False, return_tensors="pt")

generated_tokens = model.generate(
    **data_encoded,
    max_length=MAX_LENGTH_OUTPUT,
    early_stopping=True,
    num_return_sequences=4,
    num_beams=NUM_BEAMS,
)

decoded_preds = tokenizer.batch_decode(generated_tokens, skip_special_tokens=True)

pass
