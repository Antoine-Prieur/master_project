# -----------------------------------------------------------------------------+
#
#
# -----------------------------------------------------------------------------+

import os
import types
import time
from pathlib import Path
import math
from tqdm import tqdm

import torch
import torch.nn as nn

import astunparse

from transformers import PreTrainedTokenizerFast
from datasets import load_metric


import wandb

from settings import (
    DEVICE,
    LEARNING_RATE,
    EPOCHS,
    BATCH_SIZE,
    KEEP_WEIGHTS,
    MODEL,
    GRAD_ACCUM_STEPS,
    TRAINING_MODE,
    PRETRAINED,
    PRETRAINED_TOK,
    DATASET,
    GRAMMAR_PATH,
    MAX_INPUT_SIZE,
    PATH_NAME,
    FILE_NAME,
    IGNORE_PAD_INDEX,
    SCHEDULER,
    AST,
    LEAVES,
    D_MODEL,
    N_HEAD,
    N_ENC,
    N_DEC,
    CHEATER,
    TOKENIZER,
    NUM_BEAMS,
    MAX_LENGTH_OUTPUT,
    EMBED_LAP_DIM,
)
from asdl_pack.asdl import ASDLGrammar
from asdl_pack.transition_system import GenTokenAction, ApplyRuleAction, ReduceAction
from asdl_pack.lang.py3.py3_transition_system import Python3TransitionSystem
from asdl_pack.lang.py3.py3_asdl_helper import *

from dataset.dataset import AnnotedCodeDataset, AnnotedCodeCollator

from models.transformers import Transformer

from utils.arg_parser import init_args
from utils.beam_search import BeamSearch


# -----------------------------------------------------------------------------+
#
#
# -----------------------------------------------------------------------------+

grammar = ASDLGrammar.from_text(open(GRAMMAR_PATH).read())
transition_system = Python3TransitionSystem(grammar)

if DATASET == "django":
    from dataset.django import DjangoDataset

    djangoset = DjangoDataset.default_init()
    train_data, valid_data, test_data = djangoset.get_sets()

elif DATASET == "django2":
    from dataset.django import DjangoDataset

    djangoset = DjangoDataset.default_init2()
    train_data, valid_data, test_data = djangoset.get_sets()

elif DATASET == "conala":
    from dataset.conala import ConalaSet

    djangoset = ConalaSet.default_init()
    train_data, test_data = djangoset.get_sets()
    valid_data = test_data

else:
    raise NotImplementedError(f"The dataset {DATASET} does not exist")


vocab = None

if PRETRAINED_TOK:
    if MODEL == "gpt2":
        from transformers import GPT2Tokenizer

        tokenizer = GPT2Tokenizer.from_pretrained(
            PRETRAINED_TOK, local_files_only=True
        )  # télécharger dans un premier temps les poids, puis laisser à True pour ne pas avoir d'erreur sur les machines offline
        tokenizer.add_special_tokens({"sep_token": "<SEP>"})
        tokenizer.add_special_tokens({"pad_token": "<PAD>"})
    elif MODEL == "bart":
        from transformers import BartTokenizer

        tokenizer = BartTokenizer.from_pretrained(PRETRAINED_TOK, local_files_only=True)

    elif MODEL == "marian":
        from transformers import MarianTokenizer

        tokenizer = MarianTokenizer.from_pretrained(
            PRETRAINED_TOK, local_files_only=True
        )

    if DATASET == "django2":
        tokenizer.add_tokens([f"<VAR:{i:02}>" for i in range(12)])
    if AST:
        tokenizer.add_tokens(
            [
                f"<NODE_{grammar.type2id[value.type]}_{key}>"
                for key, value in grammar.id2prod.items()
            ]
        )
        tokenizer.add_tokens(["<REDUCE>"])


else:
    if MODEL in ["default_transformer", "marian", "bart", "bartLAP"]:
        tokenizer = PreTrainedTokenizerFast(
            tokenizer_file=os.path.join(
                "weights",
                "tokenizer",
                f"{TOKENIZER}_{DATASET}_AST{AST}.json",
            ),
            bos_token="<SOS>",
            eos_token="<EOS>",
            pad_token="<PAD>",
        )
    elif MODEL in ["gpt2"]:  # si on a seulement un encodeur, on utilise un séparateur
        tokenizer = PreTrainedTokenizerFast(
            tokenizer_file=os.path.join(
                "weights", "tokenizer", f"{TOKENIZER}_{DATASET}_AST{AST}.json"
            ),
            bos_token="<SOS>",
            eos_token="<EOS>",
            pad_token="<PAD>",
            sep_token="<SEP>",
        )
    else:
        raise NotImplementedError(f"The model {MODEL} does not exist")

if AST:
    if MODEL in ["bartLAP"]:
        beam_search_ast = BeamSearch(
            tokenizer, grammar, device=DEVICE, lap_pos_dim=EMBED_LAP_DIM
        )
    else:
        beam_search_ast = BeamSearch(tokenizer, grammar, device=DEVICE)

pad_id = tokenizer.pad_token_id
vocab_size = len(tokenizer)

train_set = AnnotedCodeDataset(train_data, tokenizer, grammar)
valid_set = AnnotedCodeDataset(valid_data, tokenizer, grammar)
test_set = AnnotedCodeDataset(test_data, tokenizer, grammar)

if MODEL in [
    "default_transformer",
    "marian",
    "bart",
]:  # si le modèle a un décodeur
    collate_fn = AnnotedCodeCollator(pad_id=pad_id, de_pad_id=pad_id)
    collate_fn_valid = AnnotedCodeCollator(pad_id=pad_id, de_pad_id=pad_id, valid=True)
elif MODEL in ["bartLAP"]:
    collate_fn = AnnotedCodeCollator(
        pad_id=pad_id, de_pad_id=pad_id, lap_pos_dim=EMBED_LAP_DIM
    )
    collate_fn_valid = AnnotedCodeCollator(
        pad_id=pad_id, de_pad_id=pad_id, valid=True, lap_pos_dim=EMBED_LAP_DIM
    )
elif MODEL in ["gpt2"]:
    collate_fn = AnnotedCodeCollator(pad_id=en_pad_id, sep_id=tokenizer.sep_token_id)
    collate_fn_valid = AnnotedCodeCollator(
        pad_id=pad_id, sep_id=tokenizer.sep_token_id, valid=True
    )
else:
    raise NotImplementedError(f"The model {MODEL} does not exist")

train_loader = torch.utils.data.DataLoader(
    dataset=train_set, batch_size=BATCH_SIZE, shuffle=True, collate_fn=collate_fn
)
valid_loader = torch.utils.data.DataLoader(
    dataset=valid_set, batch_size=BATCH_SIZE, shuffle=True, collate_fn=collate_fn_valid
)
test_loader = torch.utils.data.DataLoader(
    dataset=test_set, batch_size=BATCH_SIZE, shuffle=True, collate_fn=collate_fn
)

if MODEL in ["default_transformer"]:  # custom models
    model = Transformer(
        vocab_size=vocab_size,
        prod_size=vocab_size,
        d_model=D_MODEL,
        N_en=N_ENC,
        N_de=N_DEC,
        nhead=N_HEAD,
    )

# Huggingface models
elif MODEL == "gpt2":  # huggingface models
    from transformers import GPT2Config, GPT2LMHeadModel

    if PRETRAINED:

        model = GPT2LMHeadModel.from_pretrained(
            PRETRAINED, local_files_only=True
        )  # télécharger dans un premier temps les poids, puis laisser à True pour ne pas avoir d'erreur sur les machines offline
        model.resize_token_embeddings(vocab_size)
    else:
        config = GPT2Config(
            vocab_size=vocab_size,
            n_positions=MAX_INPUT_SIZE,
            n_ctx=MAX_INPUT_SIZE,
            n_embd=D_MODEL,
            n_layer=N_ENC,
            n_head=N_HEAD,
        )
        model = GPT2LMHeadModel(config)

elif MODEL == "bart":
    from transformers import BartConfig, BartForConditionalGeneration

    if PRETRAINED:
        model = BartForConditionalGeneration.from_pretrained(
            PRETRAINED, local_files_only=True
        )
        model.resize_token_embeddings(vocab_size)
    else:
        config = BartConfig(
            vocab_size=vocab_size,
            d_model=D_MODEL,
            encoder_layers=N_ENC,
            decoder_layers=N_DEC,
            encoder_attention_heads=N_HEAD,
            decoder_attention_head=N_HEAD,
            max_position_embeddings=MAX_INPUT_SIZE,
        )
        model = BartForConditionalGeneration(config)

elif MODEL == "bartLAP":
    from models.bartLAP import get_bartLAP
    from transformers import BartConfig, BartForConditionalGeneration

    if PRETRAINED:
        model = get_bartLAP(EMBED_LAP_DIM, PRETRAINED)
        model.resize_token_embeddings(vocab_size)
    else:
        config = BartConfig(
            vocab_size=vocab_size,
            d_model=D_MODEL,
            encoder_layers=N_ENC,
            decoder_layers=N_DEC,
            encoder_attention_heads=N_HEAD,
            decoder_attention_head=N_HEAD,
            max_position_embeddings=MAX_INPUT_SIZE,
        )
        model = get_bartLAP(EMBED_LAP_DIM, config=config)
        model.resize_token_embeddings(vocab_size)

elif MODEL == "marian":
    from transformers import MarianConfig, MarianMTModel

    if PRETRAINED:

        model = MarianMTModel.from_pretrained(PRETRAINED, local_files_only=True)
        model.resize_token_embeddings(vocab_size)
    else:
        config = MarianConfig(
            vocab_size=vocab_size,
            d_model=D_MODEL,
            encoder_layers=N_ENC,
            decoder_layers=N_DEC,
            encoder_attention_heads=N_HEAD,
            decoder_attention_head=N_HEAD,
            max_position_embeddings=MAX_INPUT_SIZE,
        )
        model = MarianMTModel(config)
else:
    raise NotImplementedError(f"The model {MODEL} does not exist")

print(
    f"Trainable parameters : {sum(p.numel() for p in model.parameters() if p.requires_grad)}"
)
metric = load_metric("sacrebleu")

model.to(DEVICE)

if os.path.exists(os.path.join(PATH_NAME, f"{FILE_NAME}.pt")):
    print("Weights found")
    model.load_state_dict(
        torch.load(os.path.join(PATH_NAME, f"{FILE_NAME}.pt"), map_location=DEVICE)
    )
else:
    raise ValueError("Weights not found")


def evaluate(num_beams):
    model.eval()
    with torch.no_grad():
        for i, batch in enumerate(tqdm(valid_loader)):
            data, str_map, labels = batch

            if AST:
                generated_tokens = beam_search_ast.generate(
                    model,
                    data,
                    max_length=10,
                    num_returned_sequence=1,
                    num_beams=num_beams,
                )

                decoded_preds = []
                for j in range(len(generated_tokens)):
                    try:
                        decoded_preds.append(
                            astunparse.unparse(
                                asdl_ast_to_python_ast(
                                    generated_tokens[j][2][0].tree, grammar
                                )
                            ).strip()
                        )
                    except:
                        decoded_preds.append("")
            else:
                generated_tokens = model.generate(
                    data["input_ids"],
                    attention_mask=data["attention_mask"],
                    max_length=MAX_LENGTH_OUTPUT,
                    num_beams=num_beams,
                    eos_token_id=tokenizer.eos_token_id,
                    bos_token_id=tokenizer.bos_token_id,
                    pad_token_id=tokenizer.pad_token_id,
                )

                decoded_preds = tokenizer.batch_decode(
                    generated_tokens, skip_special_tokens=True
                )

            decoded_labels = list(labels)
            for j in range(len(decoded_preds)):
                for token, value in str_map[j].items():
                    decoded_preds[j] = decoded_preds[j].replace(token, value)
                    decoded_labels[j] = decoded_labels[j].replace(token, value)

            decoded_preds = [pred.strip() for pred in decoded_preds]
            decoded_labels = [[label.strip()] for label in decoded_labels]

            metric.add_batch(predictions=decoded_preds, references=decoded_labels)

            if i == len(valid_loader) - 1:
                with torch.no_grad():

                    examples_in = tokenizer.batch_decode(
                        data["input_ids"], skip_special_tokens=True
                    )

                    examples = [
                        [examples_in[k], decoded_preds[k], decoded_labels[k][0]]
                        for k in range(len(examples_in))
                    ]

                    wandb.log(
                        {
                            "valid/examples": wandb.Table(
                                data=examples,
                                columns=["Description", "Code généré", "Code attendu"],
                            ),
                        }
                    )
    valid_metric = metric.compute()
    return valid_metric


if __name__ == "__main__":
    best_score = float("-inf")
    # best_loss = float("inf")

    for i in range(EPOCHS):

        start_time = time.time()
        loss = train()
        valid_metric = evaluate()

        if valid_metric["score"] > best_score:
            best_score = valid_metric["score"]
            torch.save(
                model.state_dict(),
                os.path.join(PATH_NAME, f"{FILE_NAME}.pt"),
            )
        """

        if loss < best_loss:
            best_loss = loss
            torch.save(
                model.state_dict(),
                os.path.join(PATH_NAME, f"{FILE_NAME}.pt"),
            )
        """
        wandb.log({"valid/BLEU": valid_metric["score"]})

        print(
            f" {i}/{EPOCHS} | BLEU score valid : {valid_metric['score']:5.2f} | epoch time : {time.time() - start_time:2.2f}s | learning rate : {optimizer.param_groups[0]['lr']}"
        )
        """
        print(
            f" {i}/{EPOCHS} | epoch time : {time.time() - start_time:2.2f}s | learning rate : {optimizer.param_groups[0]['lr']}"
        )
        """
