# -----------------------------------------------------------------------------+
#                            TEST MODELS
#
# -----------------------------------------------------------------------------+

import os
import torch
from tqdm import tqdm
import astunparse
import re
import argparse
import random

from transformers import PreTrainedTokenizerFast
from datasets import load_metric

from asdl_pack.asdl import ASDLGrammar
from asdl_pack.transition_system import GenTokenAction, ApplyRuleAction, ReduceAction
from asdl_pack.lang.py3.py3_transition_system import Python3TransitionSystem
from asdl_pack.lang.py3.py3_asdl_helper import *
from dataset.dataset import AnnotedCodeDataset, AnnotedCodeCollator

from models.transformers import Transformer

from utils.beam_search import BeamSearch


def test(
    model,
    tokenizer,
    test_loader,
    num_beams,
    max_length_output,
    grammar,
    ast,
    beam_search_ast,
):
    model.eval()
    example_list = []
    score = 0
    score_compile = 0
    with torch.no_grad():
        for i, batch in enumerate(tqdm(test_loader)):
            data, str_map, labels = batch

            if ast:
                generated_tokens = beam_search_ast.generate(
                    model,
                    data,
                    max_length=max_length_output,
                    num_returned_sequence=1,
                    num_beams=num_beams,
                )

                decoded_preds = []
                for j in range(len(generated_tokens)):
                    try:
                        decoded_preds.append(
                            astunparse.unparse(
                                asdl_ast_to_python_ast(
                                    generated_tokens[j][2][0].tree, grammar
                                )
                            ).strip()
                        )
                    except:
                        decoded_preds.append("")
            else:
                generated_tokens = model.generate(
                    data["input_ids"],
                    attention_mask=data["attention_mask"],
                    max_length=max_length_output,
                    num_beams=num_beams,
                    eos_token_id=tokenizer.eos_token_id,
                    bos_token_id=tokenizer.bos_token_id,
                    pad_token_id=tokenizer.pad_token_id,
                )

                decoded_preds = tokenizer.batch_decode(
                    generated_tokens, skip_special_tokens=True
                )

            decoded_labels = list(labels)
            for j in range(len(decoded_preds)):
                for token, value in str_map[j].items():
                    decoded_preds[j] = decoded_preds[j].replace(token, value)
                    decoded_labels[j] = decoded_labels[j].replace(token, value)

            decoded_preds = [pred.strip() for pred in decoded_preds]
            decoded_labels = [[label.strip()] for label in decoded_labels]

            # vérifier
            if decoded_labels[0][0] == decoded_preds[0]:
                score += 1

            try:
                compile(decoded_preds[0], "sumstring", "exec")
            except:
                pass
            else:
                score_compile += 1

            metric.add_batch(predictions=decoded_preds, references=decoded_labels)

            if i % (len(test_loader) // 20) == 0:
                with torch.no_grad():

                    examples_in = tokenizer.batch_decode(
                        data["input_ids"], skip_special_tokens=True
                    )

                    examples = [
                        [examples_in[k], decoded_preds[k], decoded_labels[k][0]]
                        for k in range(len(examples_in))
                    ]

                    example_list.append(examples)

    test_metric = metric.compute()
    return (
        test_metric,
        score / len(test_loader),
        score_compile / len(test_loader),
        example_list,
    )


if __name__ == "__main__":
    args = get_parser()

    class params:
        DEVICE = (
            torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
        )

        _, MODEL, DATASET, NAME = args.path.split("/")

        NUM_BEAMS = 8
        MAX_LENGTH_OUTPUT = 100
        MAX_INPUT_SIZE = 1000

        AST = bool(re.findall("_AST(.*?)_", NAME)[0])
        IGNORE_PAD_INDEX = bool(re.findall("_IPI(.*?)_", NAME)[0])

        PRETRAINED_TOK = re.findall("PRETRAINEDTOK_(.*?)_", NAME)[0].replace(
            "-", "/", 1
        )

        list_pretrained = re.findall("PRETRAINED_(.*?)_", NAME)
        PRETRAINED = list_pretrained[0].replace("-", "/", 1) if list_pretrained else ""

        list_embedlap = re.findall("LAPDIM_(.*?).pt", NAME)
        EMBED_LAP_DIM = int(list_embedlap[0]) if list_embedlap else None

        BATCH_SIZE = 1
        TOKENIZER = "BPE"

        D_MODEL = int(re.findall("_DMOD(.*?)_", NAME)[0]) if not PRETRAINED else None
        N_ENC = int(re.findall("_NENC(.*?)_", NAME)[0]) if not PRETRAINED else None
        N_DEC = int(re.findall("_NDEC(.*?)_", NAME)[0]) if not PRETRAINED else None
        N_HEAD = int(re.findall("_NHEAD(.*?)_", NAME)[0]) if not PRETRAINED else None

        PATH = os.path.join("weights", MODEL, DATASET, NAME)
        GRAMMAR_PATH = "asdl_pack/lang/py3/py385_asdl.txt"

    grammar = ASDLGrammar.from_text(open(params.GRAMMAR_PATH).read())
    transition_system = Python3TransitionSystem(grammar)

    if params.DATASET == "django":
        from dataset.django import DjangoDataset

        djangoset = DjangoDataset.default_init()
        train_data, valid_data, test_data = djangoset.get_sets()

    elif params.DATASET == "django2":
        from dataset.django import DjangoDataset

        djangoset = DjangoDataset.default_init2()
        train_data, valid_data, test_data = djangoset.get_sets()

    elif params.DATASET == "django3":
        from dataset.django import DjangoDataset

        djangoset = DjangoDataset.default_init3()
        train_data, valid_data, test_data = djangoset.get_sets()

    elif params.DATASET == "conala":
        from dataset.conala import ConalaSet

        djangoset = ConalaSet.default_init()
        train_data, test_data = djangoset.get_sets()
        valid_data = test_data

    else:
        raise NotImplementedError(f"The dataset {params.DATASET} does not exist")

    vocab = None

    if params.PRETRAINED_TOK:
        if params.MODEL == "gpt2":
            from transformers import GPT2Tokenizer

            tokenizer = GPT2Tokenizer.from_pretrained(
                params.PRETRAINED_TOK, local_files_only=True
            )  # télécharger dans un premier temps les poids, puis laisser à True pour ne pas avoir d'erreur sur les machines offline
            tokenizer.add_special_tokens({"sep_token": "<SEP>"})
            tokenizer.add_special_tokens({"pad_token": "<PAD>"})
        elif params.MODEL in ["bart", "bartLAP"]:
            from transformers import BartTokenizer

            tokenizer = BartTokenizer.from_pretrained(
                params.PRETRAINED_TOK, local_files_only=True
            )

        elif params.MODEL == "marian":
            from transformers import MarianTokenizer

            tokenizer = MarianTokenizer.from_pretrained(
                params.PRETRAINED_TOK, local_files_only=True
            )

        if params.DATASET in ["django", "django2"]:
            tokenizer.add_tokens([f"<STR:{i:02}>" for i in range(100)])
        if params.DATASET == "django2":
            tokenizer.add_tokens([f"<VAR:{i:02}>" for i in range(12)])
        if params.AST:
            tokenizer.add_tokens(
                [
                    f"<NODE_{grammar.type2id[value.type]}_{key}>"
                    for key, value in grammar.id2prod.items()
                ]
            )
            tokenizer.add_tokens(["<REDUCE>"])

    else:
        if params.MODEL in [
            "default_transformer",
            "marian",
            "bart",
            "bartLAP",
            "bartLAP2",
        ]:
            tokenizer = PreTrainedTokenizerFast(
                tokenizer_file=os.path.join(
                    "weights",
                    "tokenizer",
                    f"{params.TOKENIZER}_{params.DATASET}_AST{params.AST}.json",
                ),
                bos_token="<SOS>",
                eos_token="<EOS>",
                pad_token="<PAD>",
            )
        elif params.MODEL in [
            "gpt2"
        ]:  # si on a seulement un encodeur, on utilise un séparateur
            tokenizer = PreTrainedTokenizerFast(
                tokenizer_file=os.path.join(
                    "weights",
                    "tokenizer",
                    f"{params.TOKENIZER}_{params.DATASET}_AST{params.AST}.json",
                ),
                bos_token="<SOS>",
                eos_token="<EOS>",
                pad_token="<PAD>",
                sep_token="<SEP>",
            )
        else:
            raise NotImplementedError(f"The model {params.MODEL} does not exist")

    if params.AST:
        if params.MODEL in ["bartLAP", "bartLAP2"]:
            beam_search_ast = BeamSearch(
                tokenizer,
                grammar,
                device=params.DEVICE,
                lap_pos_dim=params.EMBED_LAP_DIM,
            )
        else:
            beam_search_ast = BeamSearch(tokenizer, grammar, device=params.DEVICE)

    pad_id = tokenizer.pad_token_id
    vocab_size = len(tokenizer)

    test_set = AnnotedCodeDataset(test_data, tokenizer, params, grammar=grammar)

    if params.MODEL in [
        "default_transformer",
        "marian",
        "bart",
    ]:  # si le modèle a un décodeur
        collate_fn = AnnotedCodeCollator(params, pad_id=pad_id, de_pad_id=pad_id)
        collate_fn_valid = AnnotedCodeCollator(
            params, pad_id=pad_id, de_pad_id=pad_id, valid=True
        )
    elif params.MODEL in ["bartLAP", "bartLAP2"]:
        collate_fn = AnnotedCodeCollator(
            params, pad_id=pad_id, de_pad_id=pad_id, lap_pos_dim=params.EMBED_LAP_DIM
        )
        collate_fn_valid = AnnotedCodeCollator(
            params,
            pad_id=pad_id,
            de_pad_id=pad_id,
            valid=True,
            lap_pos_dim=params.EMBED_LAP_DIM,
        )
    elif params.MODEL in ["gpt2"]:
        collate_fn = AnnotedCodeCollator(
            params, pad_id=pad_id, sep_id=tokenizer.sep_token_id
        )
        collate_fn_valid = AnnotedCodeCollator(
            params, pad_id=pad_id, sep_id=tokenizer.sep_token_id, valid=True
        )
    else:
        raise NotImplementedError(f"The model {params.MODEL} does not exist")

    test_loader = torch.utils.data.DataLoader(
        dataset=test_set,
        batch_size=params.BATCH_SIZE,
        shuffle=False,
        collate_fn=collate_fn,
    )

    if params.MODEL in ["default_transformer"]:  # custom models
        model = Transformer(
            vocab_size=vocab_size,
            prod_size=vocab_size,
            d_model=params.D_MODEL,
            N_en=params.N_ENC,
            N_de=params.N_DEC,
            nhead=params.N_HEAD,
        )

    # Huggingface models
    elif params.MODEL == "gpt2":  # huggingface models
        from transformers import GPT2Config, GPT2LMHeadModel

        if params.PRETRAINED:

            model = GPT2LMHeadModel.from_pretrained(
                params.PRETRAINED, local_files_only=True
            )  # télécharger dans un premier temps les poids, puis laisser à True pour ne pas avoir d'erreur sur les machines offline
            model.resize_token_embeddings(vocab_size)
        else:
            config = GPT2Config(
                vocab_size=vocab_size,
                n_positions=params.MAX_INPUT_SIZE,
                n_ctx=params.MAX_INPUT_SIZE,
                n_embd=params.D_MODEL,
                n_layer=params.N_ENC,
                n_head=params.N_HEAD,
            )
            model = GPT2LMHeadModel(config)

    elif params.MODEL == "bart":
        from transformers import BartConfig, BartForConditionalGeneration

        if params.PRETRAINED:
            model = BartForConditionalGeneration.from_pretrained(
                params.PRETRAINED, local_files_only=True
            )
            model.resize_token_embeddings(vocab_size)
        else:
            config = BartConfig(
                vocab_size=vocab_size,
                d_model=params.D_MODEL,
                encoder_layers=params.N_ENC,
                decoder_layers=params.N_DEC,
                encoder_attention_heads=params.N_HEAD,
                decoder_attention_head=params.N_HEAD,
                max_position_embeddings=params.MAX_INPUT_SIZE,
            )
            model = BartForConditionalGeneration(config)

    elif params.MODEL == "bartLAP":
        from models.bartLAP import get_bartLAP
        from transformers import BartConfig, BartForConditionalGeneration

        if params.PRETRAINED:
            model = get_bartLAP(params.EMBED_LAP_DIM, params.PRETRAINED)
            model.resize_token_embeddings(vocab_size)
        else:
            config = BartConfig(
                vocab_size=vocab_size,
                d_model=params.D_MODEL,
                encoder_layers=params.N_ENC,
                decoder_layers=params.N_DEC,
                encoder_attention_heads=params.N_HEAD,
                decoder_attention_head=params.N_HEAD,
                max_position_embeddings=params.MAX_INPUT_SIZE,
            )
            model = get_bartLAP(params.EMBED_LAP_DIM, config=config)
            model.resize_token_embeddings(vocab_size)

    elif params.MODEL == "bartLAP2":
        from models.bartLAP2 import get_bartLAP2
        from transformers import BartConfig, BartForConditionalGeneration

        if params.PRETRAINED:
            model = get_bartLAP2(params.EMBED_LAP_DIM, params.PRETRAINED)
            model.resize_token_embeddings(vocab_size)
        else:
            config = BartConfig(
                vocab_size=vocab_size,
                d_model=params.D_MODEL,
                encoder_layers=params.N_ENC,
                decoder_layers=params.N_DEC,
                encoder_attention_heads=params.N_HEAD,
                decoder_attention_head=params.N_HEAD,
                max_position_embeddings=params.MAX_INPUT_SIZE,
            )
            model = get_bartLAP2(params.EMBED_LAP_DIM, config=config)
            model.resize_token_embeddings(vocab_size)

    elif params.MODEL == "marian":
        from transformers import MarianConfig, MarianMTModel

        if params.PRETRAINED:

            model = MarianMTModel.from_pretrained(
                params.PRETRAINED, local_files_only=True
            )
            model.resize_token_embeddings(vocab_size)
        else:
            config = MarianConfig(
                vocab_size=vocab_size,
                d_model=params.D_MODEL,
                encoder_layers=params.N_ENC,
                decoder_layers=params.N_DEC,
                encoder_attention_heads=params.N_HEAD,
                decoder_attention_head=params.N_HEAD,
                max_position_embeddings=params.MAX_INPUT_SIZE,
            )
            model = MarianMTModel(config)
    else:
        raise NotImplementedError(f"The model {params.MODEL} does not exist")

    print(
        f"Trainable parameters : {sum(p.numel() for p in model.parameters() if p.requires_grad)}"
    )
    metric = load_metric("sacrebleu")

    model.load_state_dict(
        torch.load(
            params.PATH,
            map_location=params.DEVICE,
        )
    )

    model.to(params.DEVICE)

    bleu_metric, acc, compile_acc, example_list = test(
        model,
        tokenizer,
        test_loader,
        params.NUM_BEAMS,
        params.MAX_LENGTH_OUTPUT,
        grammar,
        params.AST,
        beam_search_ast,
    )
    print(
        f"BLEU : {bleu_metric}, Accuracy : {acc}, Compilation Accuracy : {compile_acc}"
    )
    print("-------------------------")
    for example in example_list:
        print(f"{example}\n")
        print("---")
