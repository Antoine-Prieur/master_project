from asdl_pack.asdl import ASDLGrammar
from asdl_pack.transition_system import GenTokenAction
from dataset.django import DjangoDataset
from settings import AST, DATASET, GRAMMAR_PATH, TOKENIZER
from transformers import BartTokenizer, GPT2Tokenizer, PreTrainedTokenizerFast

grammar = ASDLGrammar.from_text(open(GRAMMAR_PATH).read())


tokenizer1 = PreTrainedTokenizerFast(
    tokenizer_file=os.path.join(
        "weights",
        "tokenizer",
        "BPE_django2_ASTTrue.json",
    ),
    bos_token="<SOS>",
    eos_token="<EOS>",
    pad_token="<PAD>",
)

tokenizer2 = PreTrainedTokenizerFast(
    tokenizer_file=os.path.join(
        "weights",
        "tokenizer",
        "BPE_django_ASTTrue.json",
    ),
    bos_token="<SOS>",
    eos_token="<EOS>",
    pad_token="<PAD>",
)

print(1)
